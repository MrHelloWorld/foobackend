/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var resetInput = function (uid, value)
{
    let sel1 = "input[name='" + uid + "']";
    let sel2 = '#_' + uid;

    document.querySelector(sel1).value = value;
    document.querySelector(sel2).textContent = value;
};

function editButtonClick(uid)
{
    let sel1 = "input[name='" + uid + "']";
    let sel2 = '#_' + uid;

    document.querySelector(sel1).value = document.querySelector(sel2).textContent;
}

function acceptButtonClick(uid)
{
    let sel1 = "input[name='" + uid + "']";
    let sel2 = '#_' + uid;

    document.querySelector(sel2).textContent = document.querySelector(sel1).value;
}

function addFile(uid, token, storageType, fileType, destId=0)
{
    let oReq = new XMLHttpRequest();
    let oFormData = new FormData();
    let oFileInput = document.querySelector('[name="'+uid+'addedFile"]');
    let oStatusDiv = document.querySelector("#"+uid+"filesStatus");
    let oFiles = document.querySelector("#"+uid+"files");

    if (oFileInput.files[0].size > 10*1024*1024) {
        oStatusDiv.innerHTML = 'Файл очень большой. ';
        return;
    }
    oFormData.append('_token', token);
    oFormData.append('storageType', storageType);
    oFormData.append('fileType', fileType);
    oFormData.append('destId', destId);
    oFormData.append('file', oFileInput.files[0]);

    oReq.open("POST", "/files/add", true);
    oReq.send(oFormData);
    oReq.onload = (oEvent) => {
        oFileInput.value = null;
        if (oReq.status === 200) {
            oFiles.innerHTML = oReq.responseText;
            oStatusDiv.innerHTML = '';
        } else {
            oStatusDiv.innerHTML = oReq.status;
            oFiles.innerHTML = "Error "+ oReq.responseText;
        }
    };
    return false;
}

function deleteFile(uid, token, storageType, fileType, fileName, destId=0)
{
    let oReq = new XMLHttpRequest();
    let oFormData = new FormData();
    let oStatusDiv = document.querySelector("#"+uid+"filesStatus");
    let oFiles = document.querySelector("#"+uid+"files");

    oFormData.append('_token', token);
    oFormData.append('storageType', storageType);
    oFormData.append('fileType', fileType);
    oFormData.append('destId', destId);
    oFormData.append('fileName', fileName);

    oReq.open("POST", "/files/delete", true);
    oReq.
    oReq.send(oFormData);
    oReq.onload = (oEvent) => {
        if (oReq.status === 200) {
            oFiles.innerHTML = oReq.responseText;
            oStatusDiv.innerHTML = '';
        } else {
            oStatusDiv.innerHTML = oReq.status;
            oFiles.innerHTML = "Error "+ oReq.responseText;
        }
    };
    return false;
}
