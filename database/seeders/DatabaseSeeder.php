<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('investors')->delete();
        DB::table('account_operations')->delete();
        DB::table('cars')->delete();
        DB::table('resale_projects')->delete();
        DB::table('working_orders')->delete();

        DB::table('users')->where('email', '1@2.3')->count() == 0 ?
            DB::table('users')->insert([
                'email' => '1@2.3',
                'password' => Hash::make('1'),
                'name' => 'Main',
                'surname' => 'Administrator',
                'position' => 'CEO',
                'role' => User::USER_ROLE_ADMIN,
            ])
            : null;
        $this->call([
            UserSeeder::class,
            InvestorSeeder::class,
            CarSeeder::class,
            ResaleProjectSeeder::class,
        ]);
    }
}
