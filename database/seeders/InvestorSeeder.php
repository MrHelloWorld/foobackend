<?php

namespace Database\Seeders;

use App\Models\AccountOperation;
use App\Models\Investor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;

class InvestorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Investor::factory()->count(10)->create()->each(
            function ($investor) {
                AccountOperation::factory()->create([
                    'investor_id' => $investor->id
                ]);
            }
        );
    }
}
