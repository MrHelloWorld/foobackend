<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\ResaleProject;
use App\Models\User;
use App\Models\WorkingOrder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResaleProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = Car::all()->shuffle();
        $sellers = User::all();
        $maxProjects = rand(0, count($cars));
        $car = $cars->last();

        ResaleProject::factory([
            'car_id' => $car->id,
            'buy_price' => $car->buy_price
        ])->count($maxProjects)->create()->each(
            function ($project) use ($cars, $car , $sellers) {
                $car = $cars->pop();
                for ($j = 0; $j < rand(1, 10); $j++) {
                    WorkingOrder::factory()->create([
                        'resale_project_id' => $project->id,
                        'created_user_id' => $project->buyer_id
                    ]);
                }
                if (rand(0, 1)) {
                    $sellPrice = $car->buy_price + rand(380, 600) * 1000;;
                    WorkingOrder::factory()->create([
                        'resale_project_id' => $project->id,
                        'type' => WorkingOrder::ORDER_TYPE_SALE,
                        'created_user_id' => $project->buyer_id,
                        'price' => $sellPrice,
                    ]);
                    $project->seller_id = $sellers->random()->id;
                    $project->sell_price = $sellPrice;
                    $project->sell_datetime = now()->addHours(rand(24, 72));
                }
                $project->buy_price = $car->buy_price;
                $project->car_id = $car->id;
                $project->save();
            }
        );
    }
}
