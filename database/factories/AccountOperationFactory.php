<?php

namespace Database\Factories;

use App\Models\AccountOperation;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountOperationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AccountOperation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'operation_name' => "Внесение средств",
            'amount' => rand(10,99) * 100000,
            ];
    }
}
