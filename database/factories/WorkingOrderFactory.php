<?php

namespace Database\Factories;

use App\Models\WorkingOrder;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkingOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WorkingOrder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $endTime = rand(0, 1) ? now()->addHours(rand(24, 72)): null;
        return [
            'description' => $this->faker->text(),
            'start_datetime' => now(),
            'end_datetime' => $endTime,
            'type' => WorkingOrder::ORDER_TYPE_WORK,
            'price' => rand(10,90) * 1000,
        ];
    }
}
