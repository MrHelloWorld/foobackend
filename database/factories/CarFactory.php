<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;
    public const CAR_MODELS = ['Жигули', 'Волга', "Запорожец", "Нива", "Лада"];
    public const CAR_TYPES = ['Легковая/В', 'Тяжелая/Х', 'Мотоцикл/А'];
    public const CAR_COLORS = ['Красный', 'Синий', 'Фиолетовый', 'Оранжевый', 'Зеленый'];
    //public const CAR_COLORS = ['Красный', 'Синий', 'Фиолетовый', 'Оранжевый', 'Зеленый'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::where('role','<>', User::USER_ROLE_SELLER)->get();
        return [
            'created_user_id' => $users->random()->id,
            'model' => self::CAR_MODELS[array_rand(self::CAR_MODELS)],
            'year' => rand(1950, 2020),
            'type' => self::CAR_TYPES[array_rand(self::CAR_TYPES)],
            'color' => self::CAR_COLORS[array_rand(self::CAR_COLORS)],
            'reg_number' => $this->faker->randomLetter.rand(1,999).$this->faker->countryISOAlpha3,
            'vin' => $this->faker->iban(),
            'kuzov_number' => $this->faker->iban(),
            'rama_number' => $this->faker->iban(),
            'power_horses' => rand(71,536),
            'power_kvt' => rand(10,150),
            'mass' => rand(1000,10000),
            'buy_price' => rand(100,10000)*1000,
        ];
    }
}
