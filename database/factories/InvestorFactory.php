<?php

namespace Database\Factories;

use App\Models\Investor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvestorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Investor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admins = User::where('role', USER::USER_ROLE_ADMIN)->get();

        return [
            'login' => $this->faker->userName,
            'password' => '$2y$10$aITIWv7ieCWbb0JcVavXNOGw4NceFdXJ7polCYfT8fAoKYEMclvVe',
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->phoneNumber,
            'created_user_id' =>$admins->random()->id
            ];
    }
}
