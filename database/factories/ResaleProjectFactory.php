<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\Investor;
use App\Models\ResaleProject;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResaleProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ResaleProject::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admins = User::where('role', USER::USER_ROLE_ADMIN)->get();
        return [
            'investor_id' => Investor::all()->random()->id,
            'buyer_id' => $admins->random()->id,
            'buy_datetime' => now(),
            'description' => $this->faker->text(),
        ];
    }
}
