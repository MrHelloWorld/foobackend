<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;
    protected const POSITIONS = [
        'Начальник отдела' => User::USER_ROLE_ADMIN,
        'Закупщик' => User::USER_ROLE_BUYER,
        'Продавец' => User::USER_ROLE_SELLER,
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $rand = array_rand(self::POSITIONS);

        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->unique()->safeEmail,
            'password' => '$2y$10$aITIWv7ieCWbb0JcVavXNOGw4NceFdXJ7polCYfT8fAoKYEMclvVe',
            'position' => $rand,
            'role' => self::POSITIONS[$rand],
        ];
    }
}
