<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingOrdersTable extends Migration
{    
    public $timestamps = false;
    
    public function up()
    {
        Schema::create('working_orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('resale_project_id');
            $table->tinyInteger('type', false, true);
            $table->text('description')->nullable();
            $table->unsignedBigInteger('price');
            $table->foreignId('created_user_id');
            $table->dateTime('start_datetime');
            $table->dateTime('end_datetime')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }    

    public function down()
    {
        Schema::dropIfExists('working_orders');
    }
}
