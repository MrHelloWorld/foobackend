<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_user_id');
            $table->string('model', 100);
            $table->unsignedSmallInteger('year');
            $table->string('type', 100)->nullable();
            $table->string('color', 100);
            $table->string('reg_number', 100)->nullable();
            $table->string('vin', 50)->nullable();
            $table->string('kuzov_number', 50)->nullable();
            $table->string('rama_number', 50)->nullable();
            $table->unsignedSmallInteger('power_horses')->nullable();
            $table->unsignedFloat('power_kvt')->nullable();
            $table->unsignedSmallInteger('mass')->nullable();
            $table->unsignedInteger('buy_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
