<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResaleProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resale_projects', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->foreignId('investor_id');
            $table->foreignId('car_id');
            $table->foreignId('buyer_id');
            $table->unsignedDouble('buy_price');
            $table->dateTime('buy_datetime');
            $table->foreignId('seller_id')->nullable();
            $table->unsignedDouble('sell_price')->nullable();
            $table->dateTime('sell_datetime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resale_projects');
    }
}
