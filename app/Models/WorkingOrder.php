<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

class WorkingOrder extends Model
{
    use HasFactory;

    public const ORDER_TYPE_WORK = 0;
    public const ORDER_TYPE_SALE = 1;

    protected $fillable = [
        'resale_project_id',
        'type',
        'description',
        'price',
        'created_user_id',
        'start_datetime',
    ];

    protected $table = 'working_orders';
    public $timestamps = false;

    public function resale_project()
    {
        return $this->belongsTo(ResaleProject::class, 'resale_project_id');
    }

    public function created_user()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }

    public static function addNew(array $params)
    {
        self::create($params);
    }

    public function delete()
    {        
        if ($this->type == self::ORDER_TYPE_SALE) {
            $this->resale_project->sell_datetime = null;
            $this->resale_project->seller_id = null;
            $this->resale_project->sell_price = null;
            $this->resale_project->save();
        }
        return parent::delete();
    }

    public static function deleteExists(int $id)
    {
        $order = self::find($id);
        if ($order->type == self::ORDER_TYPE_SALE) {
            $order->resale_project->sell_datetime = null;
            $order->resale_project->seller_id = null;
            $order->resale_project->sell_price = null;
            $order->resale_project->save();
        }
        self::destroy($id);
    }

    public function closeOrder(string $datetime, int $price)
    {        
        $this->end_datetime = $datetime;
        $this->price = $price;
        $this->save();
    }
}
