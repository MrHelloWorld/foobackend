<?php

namespace App\Models;

use App\Events\InvestorAccountChangedEvent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountOperation extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'account_operations';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'investor_id',
        'operation_name',
        'amount',
        'timestamp'
    ];

    protected $dispatchesEvents = [
        'created' => InvestorAccountChangedEvent::class
    ];

    public function investor()
    {
        return $this->belongsTo(Investor::class, 'investor_id');
    }
}
