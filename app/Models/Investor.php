<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Investor extends Model
{
    use HasFactory;
    use HasApiTokens;

    protected $table = 'investors';

    protected $hidden = ['password'];

    protected $fillable = [
        'login',
        'password',
        'name',
        'surname',
        'email',
        'phone',
        'created_user_id',
    ];

    public function addDeposit(string $description, int $amount)
    {
        AccountOperation::create([
            'investor_id' => $this->id,
            'operation_name' => $description,
            'amount' => $amount,
        ]);
    }

    public function calcBalance(): int
    {
        $res = 0;
        $allOpers = $this->account_operation;
        foreach ($allOpers as $operation) {
            $res += $operation->amount;
        }
        $allProjs = $this->resale_project;
        if ($allProjs == null)
            return $res;
        foreach($allProjs as $project) {
            $res -= $project->buy_price;
            $allOrders = $project->working_order;
            if ($allOrders == null)
                continue;
            foreach($allOrders as $order) {
                if ($order->type == WorkingOrder::ORDER_TYPE_SALE) {
                    $res += $order->price;
                } else {
                    $res -= $order->price;
                }
            }
        }
        return $res;
    }

    public function resale_project()
    {
        return $this->hasMany(ResaleProject::class, 'investor_id');
    }

    public function account_operation()
    {
        return $this->hasMany(AccountOperation::class, 'investor_id');
    }

    public function makeDeposit(int $amount, string $operationName = 'Внесение средств')
    {
        AccountOperation::create([
            'investor_id' => $this->id,
            'operation_name' => $operationName,
            'amount' => $amount
        ]);
        return $this;
    }

    public function makeWithdraw(int $amount)
    {
        return $this->makeDeposit(-$amount, 'Вывод средств');
    }
}
