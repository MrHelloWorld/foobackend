<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
//    use HasProfilePhoto;
//    use Notifiable;
//    use TwoFactorAuthenticatable;

    public const USER_ROLE_ADMIN = 1;
    public const USER_ROLE_BUYER = 2;
    public const USER_ROLE_SELLER = 3;

    public const USER_ROLES = [
        self::USER_ROLE_ADMIN => "Администратор",
        self::USER_ROLE_BUYER => "Выкупщик",
        self::USER_ROLE_SELLER => "Продавец",
    ];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'position',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        //'two_factor_recovery_codes',
        //'two_factor_secret',
    ];

    public function getRoleText()
    {
        switch ($this->role) {
            case User::USER_ROLE_ADMIN:
                return 'администрирование';

            case User::USER_ROLE_BUYER:
                return 'управление проектом';

            case User::USER_ROLE_SELLER:
                return 'ордер на продажу';
        }

    }
}
