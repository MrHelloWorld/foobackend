<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'created_user_id',
        'model',
        'year',
        'type',
        'color',
        'reg_number',
        'vin',
        'kuzov_number',
        'rama_number',
        'power_horses',
        'power_kvt',
        'mass',
        'buy_price'
    ];

    protected $table = 'cars';

    public function resale_project()
    {
        return $this->hasOne(ResaleProject::class, 'car_id')->withDefault();
    }

    public function investor()
    {
        return $this->resaleProject->investor()->withDefaults();
    }

    public function buyer()
    {
        return $this->resale_project->buyer();
    }

    public function seller()
    {
        return $this->resale_project->seller();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }

    public static function getBuyDatetime(int $carId)
    {
        $dt = DateTime::createFromFormat("Y-m-d H:i:s", ResaleProject::select('buy_datetime')->where(['car_id' => $carId])->get()->first()->buy_datetime);
        return $dt;
    }

    public static function getCarsNotInProjects()
    {
        $cars = DB::table('cars')
                ->leftJoin('resale_projects', 'cars.id', '=', 'resale_projects.car_id')
                ->whereNull('resale_projects.id')
                ->select('cars.*')
                ->get();
        return $cars;
    }

    public function delete()
    {
        if ($this->resale_project()->exists()) {
            $this->resale_project->delete();
        }
        return parent::delete();
    }

    public static function sell(int $resalePorjectId, ?string $description, int $price, string $datetime)
    {
        $order = WorkingOrder::create([
            'resale_project_id' => $resalePorjectId,
            'type' => WorkingOrder::ORDER_TYPE_SALE,
            'description' => $description,
            'price' => $price,
            'created_user_id' => auth()->user()->id,
            'start_datetime' => $datetime
        ]);
        $rp = ResaleProject::find($resalePorjectId);
        $rp->seller_id = auth()->user()->id;
        $rp->sell_price = $price;
        $rp->sell_datetime = $datetime;
        $rp->save();
        return $order;
    }

    public function isSold()
    {
        $wo = WorkingOrder::
                where('resale_project_id', $this->resale_project->id)->
                where('type', WorkingOrder::ORDER_TYPE_SALE)->get()->first();
        return $wo == null? false: true;
    }
}
