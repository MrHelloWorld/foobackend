<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResaleProject extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'investor_id',
        'car_id',
        'buyer_id',
        'buy_price',
        'buy_datetime',
    ];

    protected $table = 'resale_projects';
    public $timestamps = false;

    public function buyer()
    {
        return $this->belongsTo(User::class, 'buyer_id')->withDefault();
    }

    public function investor()
    {
        return $this->belongsTo(Investor::class, 'investor_id')->withDefault();
    }

    public function working_order()
    {
        return $this->hasMany(WorkingOrder::class, 'resale_project_id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id')->withDefault();
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id')->withDefault();
    }

    public function delete()
    {
        if ($this->working_order()->exists()) {
            foreach ($this->working_order as $order) {
                $order->delete();
            }
        }
        return parent::delete();
    }

    public static function deleteProject(int $id)
    {
        $project = ResaleProject::find($id);

        foreach($project->working_order as $order) {
            $order->delete();
        }
        $project->delete();
        //ResaleProject::destroy($id);
    }

    public function calcProfit()
    {
        $profit = 0;
        $profitArr = [];
        foreach ($this->working_order as $order) {
            if ($order->type == WorkingOrder::ORDER_TYPE_SALE) {
                $profitArr[] = -$order->price;
                $profit += $order->price;
            } else
            {
                $profit -= $order->price;
                $profitArr[] = $order->price;
            }
        }
        $profit -= $this->buy_price;

        return $profit;
    }
}
