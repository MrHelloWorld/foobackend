<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

class File extends Model
{
    public const FILE_TYPE_RESALE_PROJECT = 1;
    public const FILE_TYPE_WORKING_ORDER = 2;
    
    public const FILE_STATUS_UPLOADED = 1;
    public const FILE_STATUS_WAITING = 2;
    
    protected $fillable = [
        'name',
        'original_name',
        'type',
        'dest_id',
        'status'
    ];
    
    protected $table = 'files';
    
}
