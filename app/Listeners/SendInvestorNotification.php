<?php

namespace App\Listeners;

use App\Events\InvestorAccountChangedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendInvestorNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object InvestorAccountChangedEvent $event
     * @return void
     */
    public function handle(InvestorAccountChangedEvent $event)
    {
        dump($event);
    }
}
