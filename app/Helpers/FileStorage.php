<?php

namespace App\Helpers;

use App\Models\File;
use Illuminate\Support\Facades\Storage;
use App\Services\FileService;
use Exception;

class FileStorage
{
    const STORAGE_TYPE_RESALE_PROJECT = File::FILE_TYPE_RESALE_PROJECT;
    const STORAGE_TYPE_WORKING_ORDER = File::FILE_TYPE_WORKING_ORDER;

    const STORAGE_FOLDER = [
        self::STORAGE_TYPE_RESALE_PROJECT => 'resale_projects',
        self::STORAGE_TYPE_WORKING_ORDER => 'working_orders'
    ];

    protected int $storageType;
    protected FileService $fileService;

    public function __construct(int $storageType)
    {
        $this->storageType = $storageType;
        $this->fileService = new FileService();
    }

    public function addTempFileFromRequest()
    {
        if (request()->file('file') == null || $this->tempFilesCount() > 3)
            return;
        try {
            $path = request()->file('file')->store(self::STORAGE_FOLDER[$this->storageType].DIRECTORY_SEPARATOR.'0');
            $this->fileService->create([
                'name' => $path,
                'original_name' => request()->file('file')->getClientOriginalName(),
                'type' => $this->storageType,
                'dest_id' => 0,
                'status' => File::FILE_STATUS_WAITING,
            ]);
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function addFileFromRequest(int $id)
    {
        if (request()->file('file') == null )
            return;
        try {
            $path = request()->file('file')->store(self::STORAGE_FOLDER[$this->storageType].DIRECTORY_SEPARATOR.$id);
            $this->fileService->create([
                'name' => $path,
                'original_name' => request()->file('file')->getClientOriginalName(),
                'type' => $this->storageType,
                'dest_id' => $id,
                'status' => File::FILE_STATUS_UPLOADED,
            ]);
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function getAllTempFiles()
    {
        return $this->fileService->getTempFiles($this->storageType);
    }

    public function getAllFiles(int $id)
    {
        return $this->fileService->getNormalFiles($id, $this->storageType);
    }

    public function tempFilesCount()
    {
        return $this->fileService->getTempFilesCount($this->storageType);
    }

    public function deleteAllTempFiles()
    {
        $files = $this->getAllTempFiles();
        foreach ($files as $file) {
            Storage::delete($file['name']);
        }
        $this->fileService->deleteTempFiles();
    }

    public function acceptAllTempFiles(int $id)
    {
        if ($this->tempFilesCount() < 1)
            return;
        $tempFiles = $this->fileService->getTempFiles($this->storageType);
        foreach ($tempFiles as $file) {
            $e = explode(DIRECTORY_SEPARATOR, $file['name']);
            $justName = end($e);
            $moveTo = self::STORAGE_FOLDER[$this->storageType].DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$justName;
            Storage::move($file['name'], $moveTo);
            $this->fileService->acceptTempFiles($this->storageType, $id, $moveTo);
        }
    }

    public static function deleteFile(string $fileName)
    {
        $file = File::where('name', $fileName)->get()->first();

        if ($file == null) {
            return;
        }
        $file->delete();
        Storage::delete($fileName);
    }

    public static function deleteAllFiles(int $fileType, int $destId)
    {
        (new FileService())->deleteAllFiles($fileType, $destId);
        Storage::deleteDirectory(self::STORAGE_FOLDER[$fileType].DIRECTORY_SEPARATOR.$destId);
    }
}
