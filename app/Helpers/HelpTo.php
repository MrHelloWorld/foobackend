<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HelpTo
 *
 * @author m
 */
namespace App\Helpers;

use DateTime;

class HelpTo 
{
    public static function getHumanDateTime(?string $dt)
    {
        if ($dt == null)
            return '';        
        return DateTime::createFromFormat("Y-m-d H:i:s", $dt)->format("d.m.Y H:i");
    }
    
    public static function getHumanDate(?string $dt)
    {
        if ($dt == null)
            return '';
        return DateTime::createFromFormat("Y-m-d H:i:s", $dt)->format("d.m.Y");
    }
    
    public static function daysBetweenDatabaseDates(string $date1, string $date2)
    {
        $dt1 = DateTime::createFromFormat("Y-m-d H:i:s", $date1);
        $dt2 = DateTime::createFromFormat("Y-m-d H:i:s", $date2);
        
        return $dt1->diff($dt2)->d;
        
        
    }
}
