<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\FileStorage;
use App\Models\File;

class FileStorageView extends Component
{
    public int $storageType;
    public int $storageFilesStatus;
    public int $id;
    public string $addButton;
    public FileStorage $storage;
    public array $allFiles;
    public bool $needAddButton;
    public $uid;

    public function __construct($storageType, $storageFilesStatus, $id=0, $addButton='')
    {
        $this->storageType = $storageType;
        $this->storageFilesStatus = $storageFilesStatus;
        $this->id = $id;
        $this->addButton = $addButton;
        $this->needAddButton = strlen($addButton) > 0? true: false;
        $this->uid = '_'.uniqid();
        $this->storage = new FileStorage($storageType);
        if ($storageFilesStatus == File::FILE_STATUS_WAITING) {
            $this->allFiles = $this->storage->getAllTempFiles();
            if (count($this->allFiles) == 0)
                return;
        } elseif ($storageFilesStatus == File::FILE_STATUS_UPLOADED) {
            $this->allFiles = $this->storage->getAllFiles($id);
            if (count($this->allFiles) == 0)
                return;
        }
    }

    public function render()
    {
        return view('components.file-storage-view');
    }
}
