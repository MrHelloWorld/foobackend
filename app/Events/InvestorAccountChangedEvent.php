<?php

namespace App\Events;

use App\Models\AccountOperation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InvestorAccountChangedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected AccountOperation $operation;
    public string $message = "";

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AccountOperation $operation)
    {
        $this->operation = $operation;
        $this->message = <<<EOT
            "Здравствуйте, {$operation->investor->name}.

            Произошли изменения на Вашем счёте, операция:
                $operation->operation_name,
                Сумма: $operation->amount";
        EOT;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
