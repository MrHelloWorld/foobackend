<?php

namespace App\Services;

use App\Models\Car;
use App\Models\ResaleProject;
use Illuminate\Support\Facades\Auth;

class CarService extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'created_user_id',
        'model',
        'year',
        'type',
        'color',
        'reg_number',
        'vin',
        'kuzov_number',
        'rama_number',
        'power_horses',
        'power_kvt',
        'mass',
        'buy_price',
        'created_at',
        'updated_at',
    ];

    public function create(array $attributes)
    {
        if (!$this->role->isAdmin() && !$this->role->isBuyer()) {
            session()->flash('error', 'Недосточно прав для добавления машины');
            return false;
        }
        return parent::create(array_merge($attributes, ['created_user_id' => Auth::user()->id]));
    }

    public function updateProperties(array $attributes, int $carId = 0)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для изменения автомобиля');
            return false;
        }
        return parent::updateProperties($attributes, $carId);
    }


    public function isSold(int $carId = 0): string
    {
        $car = $carId == 0? $this->model: Car::find($carId);
        return $car->isSold();
    }

    public function getBuyerId(int $carId = 0)
    {
        $car = $carId == 0? $this->model: Car::find($carId);

        return $car->buyer->id;
    }

    public function getSellerId(int $carId = 0)
    {
        $car = $carId == 0? $this->model: Car::find($carId);

        return $car->seller->id;
    }

    public function getResaleProjectId(int $carId = 0)
    {
        $car = $carId == 0? $this->model: Car::find($carId);

        return $car->resale_project->id;
    }

    public function delete(int $carId = 0)
    {
        $car = $carId == 0? $this->model: Car::find($carId);
        if ($car->resale_project->exists) {
            if ($this->role->isBuyer() && $car->resale_project->buyer_id != auth()->user()->id) {
                session()->flash('error', 'Вы не можете удалить машину, так как не создавали проект для нее');
                return false;
            }
            if ($this->role->isSeller()) {
                session()->flash('error', 'Продавец не может удалять автомобили для которых создан проект');
                return false;
            }
        }
        return $car->delete();
    }

    public function sell(int $resaleProjectId, $description, $price, $datetime)
    {
        if (!$this->role->isSeller() && !$this->role->isAdmin()) {
            session()->flash('error', 'Только продавцы и администраторы моугут продавать автомобили');
            return false;
        }
        return Car::sell($resaleProjectId, $description, $price, $datetime);
    }

    public function getCarPropertiesNotInProjects()
    {
        $cars = Car::getCarsNotInProjects();
        if (count($cars) == 0)
            return [];
        $retVal = [];
        foreach ($cars as $car) {
            foreach ($this->publicColumns as $column) {
                $retVal[$car->id][$column] = $car->$column;
            }
        }
        return $retVal;
    }

    public function getCarIfItBelongsToInvestor(int $carId, int $investorId)
    {
        $project = ResaleProject::where('car_id', $carId)->where('investor_id', $investorId)->get();

        return count($project) > 0 ? (new static($this->role->isInvestor()))->getProperties($carId): null;
    }
}
