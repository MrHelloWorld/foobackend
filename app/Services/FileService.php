<?php

namespace App\Services;

use App\Models\File;

class FileService //extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'name',
        'original_name',
        'type',
        'dest_id',
        'status',
        'created_at',
        'updates_at'
    ];

    protected function getFilesProperties($files)
    {
        $ret = [];
        foreach($files as $file) {
            foreach ($this->publicColumns as $column) {
                $ret[$file->id][$column] = $file->$column;
            }
        }
        return $ret;
    }

    public function create(array $attributes)
    {
        return File::create($attributes);
    }

    public function getTempFiles(int $storageType): array
    {
        $files = File::where('status', File::FILE_STATUS_WAITING)
                ->where('type', $storageType)
                ->get();

        if (count($files) == 0)
            return [];
        return $this->getFilesProperties($files);
    }

    public function getNormalFiles(int $id, int $storageType): array
    {
        $files = File::where('status', File::FILE_STATUS_UPLOADED)
                ->where('type', $storageType)
                ->where('dest_id', $id)
                ->get();

        return $this->getFilesProperties($files);
    }

    public function deleteTempFiles(int $storageType)
    {
        File::where('status', File::FILE_STATUS_WAITING)
                ->where('type', $storageType)
                ->delete();
    }

    public function acceptTempFiles(int $storageType, int $destId, string $moveTo)
    {
        $files = File::where('status', File::FILE_STATUS_WAITING)
                ->where('type', $storageType)
                ->get();

        foreach ($files as $file) {
            $file->status = File::FILE_STATUS_UPLOADED;
            $file->name = $moveTo;
            $file->dest_id = $destId;
            $file->save();
        }
    }

    public function getTempFilesCount(int $storageType)
    {
        return File::where('status', File::FILE_STATUS_WAITING)
                ->where('type', $storageType)
                ->count();
    }

    public function getProperties(int $fileType, int $destId)
    {
        $ret = [];

        $files = File::where('type', $fileType)->where('dest_id', $destId)->get();
        if (count ($files) == 0)
            return [];
        foreach($files as $file) {
            foreach ($this->publicColumns as $column) {
                $ret[$file[$file['id']]][$column] = $file->$column;
            }
        }
        return $ret;
    }

    public function deleteAllFiles(int $fileType, int $destId)
    {
        File::where('type', $fileType)->where('dest_id', $destId)->delete();
    }
}
