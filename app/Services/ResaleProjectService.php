<?php

namespace App\Services;

use App\Models\ResaleProject;
use App\Helpers\FileStorage;
use Illuminate\Support\Facades\Auth;

class ResaleProjectService extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'description',
        'investor_id',
        'car_id',
        'buyer_id',
        'buy_price',
        'buy_datetime',
        'seller_id',
        'sell_price',
        'sell_datetime',
    ];

    public function create(array $attributes)
    {
        if (!$this->role->isAdmin() && !$this->role->isBuyer()) {
            session()->flash('error', 'Недосточно прав для добавления проекта');
            return false;
        }
        //$attributes = array_merge($attributes, ['buyer_id' => Auth::user()->id]);
        return parent::create(array_merge($attributes, ['buyer_id' => Auth::user()->id]));
    }

    public function getPropertiesByCarId(int $carId)
    {
        $rp = ResaleProject::where('car_id', $carId)->first();

        if ($rp == null)
            return [];
        $service = new self();
        $service->setModel($rp);
        return $service->getProperties();
    }

    public function calcProfit(int $projectId = 0)
    {
        $project = $projectId == 0? $this->model: ResaleProject::find($projectId);

        return $project->calcProfit();
    }

    public function getCarId(int $projectId = 0)
    {
        return $this->getPropertie('car_id', $projectId);
    }

    public function delete(int $projectId = 0)
    {
        $project = $projectId == 0? $this->model: ResaleProject::find($projectId);
        if (!$this->role->isAdmin() && !($this->role->isBuyer() && auth()->user()->id == $project->id)) {
            session()->flash('error', 'Недосточно прав для удаления проекта');
            return false;
        }
        FileStorage::deleteAllFiles(FileStorage::STORAGE_TYPE_RESALE_PROJECT, $project->id);
        return $project->delete();
    }

    public function getLeftId(int $projectId = 0)
    {
        $project = $projectId == 0? $this->model: ResaleProject::find($projectId);

        return ResaleProject::where('id', '<', $project->id)->orderBy('id', 'desc')->first();
    }

    public function getRightId(int $projectId = 0)
    {
        $project = $projectId == 0? $this->model: ResaleProject::find($projectId);

        return ResaleProject::where('id', '>', $project->id)->first();
    }
}
