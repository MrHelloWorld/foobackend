<?php

namespace App\Services;

use App\Models\WorkingOrder;
use App\Models\ResaleProject;
use App\Models\Car;
use App\Helpers\FileStorage;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class WorkingOrderService extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'resale_project_id',
        'type',
        'description',
        'price',
        'created_user_id',
        'start_datetime',
        'end_datetime',
        'created_at'
    ];

    public function create(array $attributes)
    {
        if ($this->role->isSeller() && $attributes['type'] != WorkingOrder::ORDER_TYPE_SALE) {
            session()->flash('error', 'Продавец не может добавлять ордер на работы');
            return false;
        }
        $rp = ResaleProject::find($attributes['resale_project_id']);
        if ($this->role->isBuyer() && auth()->user()->id != $rp->buyder_id) {
            session()->flash('error', 'Вы не можете добавлять ордер на работы, так как не создавали проект');
            return false;
        }
        return parent::create(array_merge($attributes, ['created_user_id' => Auth::user()->id]));
    }

    public function delete(int $orderId)
    {
        $order = $orderId == 0? $this->model: WorkingOrder::find($orderId);
        $rp = ResaleProject::find($order->resale_progect_id);
        if ($this->role->isBuyer() && auth()->user()->id != $rp->buider_id) {
            session()->flash('error', 'Вы не можете удалить ордер на работы, так как не создавали проект');
            return false;
        }
        FileStorage::deleteAllFiles(FileStorage::STORAGE_TYPE_WORKING_ORDER, $order->id);
        return $order->delete();
    }

    public function getPropertiesForCar(int $carId): array
    {
        $orders = Car::find($carId)->resale_project->working_order;
        if (!$orders) {
            return [];
        }
        $ret = [];
        foreach ($orders as $order) {
            foreach ($this->publicColumns as $column) {
                $ret[$order->id][$column] = $order->$column;
            }
        }
        return $ret;
    }

    public function closeOrder($endDatetime, int $price, int $orderId)
    {
        $order = $orderId == 0? $this->model: WorkingOrder::find($orderId);
        $rp = ResaleProject::find($order->resale_progect_id);
        if ($this->role->isBuyer() && auth()->user()->id != $rp->buider_id) {
            session()->flash('error', 'Вы не можете закрыть ордер на работы, так как не создавали проект');
            return false;
        }
        $order->closeOrder($endDatetime, $price);
    }
}
