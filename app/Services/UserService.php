<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserService extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'name',
        'surname',
        'email',
        'position',
        'role',
        'created_at',
        'updated_at'
    ];

    public function create(array $attributes)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для добавления пользователя');
            return false;
        }
        $attributes['password'] = Hash::make($attributes['password']);
        return parent::create($attributes);
    }

    public function updateProperties(array $attributes, int $userId = 0)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для добавления изменения пользователя');
            return false;
        }
        if (array_key_exists('password', $attributes)) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
        return parent::updateProperties($attributes, $userId);
    }

    public function changePassword(string $newPassword, int $userId)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для добавления изменения пароля');
            return false;
        }
        User::find($userId)->update(['password' => Hash::make($newPassword)]);
        return true;
    }

    public function getRole(int $userId = 0): string
    {
        $user = $userId == 0? $this->user: User::find($userId);
        return $user->getRoleText();
    }
}
