<?php

namespace App\Services;

use App\Models\User;

class RoleService
{
    protected $role;
    protected $isInvestor;

    public function __construct(bool $isInvestor = false)
    {
        if ($isInvestor) {
            $this->isInvestor = $isInvestor;
        } else {
            $this->role = auth()->user()->role;
        }
    }

    public function isAdmin()
    {
        return $this->role == User::USER_ROLE_ADMIN;
    }

    public function isBuyer()
    {
        return $this->role == User::USER_ROLE_BUYER;
    }

    public function isSeller()
    {
        return $this->role == User::USER_ROLE_SELLER;
    }

    public function isInvestor()
    {
        return $this->isInvestor;
    }
}
