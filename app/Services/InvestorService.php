<?php

namespace App\Services;

use App\Models\Investor;
use App\Models\AccountOperation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class InvestorService extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'name',
        'surname',
        'email',
        'phone',
        'created_user_id',
        'created_at',
        'updates_at'
    ];

    public function create(array $attributes, int $initialBalance = 0)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для добавления инвестора');
            return false;
        }
        $attributes['password'] = Hash::make($attributes['password']);
        $investor = parent::create(array_merge($attributes, ['created_user_id' => Auth::user()->id]));
        if ($initialBalance > 0)
            $investor->addDeposit('Внесение средств при регистрации', $initialBalance);
        return $investor;
    }

    public function makeDeposit(int $amount, string $description, int $investorId)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для внесения депозита (снятия)');
            return false;
        }
        AccountOperation::create([
            'investor_id' => $investorId,
            'operation_name' => $description,
            'amount' => $amount
        ]);
    }

    public function makeWithdraw(int $amount, string $description, int $investorId)
    {
        $this->makeDeposit(-$amount, $description, $investorId);
    }

    public function getAccountOperations(int $investorId = 0)
    {
        if (!$this->role->isAdmin() && !$this->role->isInvestor()) {
            session()->flash('error', 'Недосточно прав для получения операций по счету');
            return [];
        }
        if ($investorId == 0) {
            return $this->model->account_operation;
        } else {
            return Investor::find($investorId)->account_operation;
        }
    }

    public function getResaleProjects(int $investorId = 0)
    {
        if (!$this->role->isAdmin() &&
            !$this->role->isInvestor()) {
            session()->flash('error', 'Недосточно прав для получения списка проектов');
            return [];
        }
        if ($investorId == 0) {
            return $this->model->resale_project;
        } else {
            return Investor::find($investorId)->resale_project;
        }
    }

    public function calcBalance(int $investorId = 0): int
    {
        $investor = $investorId == 0 ? $this->model: Investor::find($investorId);

        return $investor->calcBalance();
    }

    public function updateProperties(array $attributes, int $investorId = 0)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для добавления изменения пользователя');
            return false;
        }
        if (array_key_exists('password', $attributes)) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
        return parent::updateProperties($attributes, $investorId);
    }
}
