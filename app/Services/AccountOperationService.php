<?php

namespace App\Services;

use App\Models\AccountOperation;
use Illuminate\Support\Facades\Request;

class AccountOperationService extends BaseRoleService
{
    protected array $publicColumns = [
        'id',
        'investor_id',
        'operation_name',
        'amount',
        'timestamp'
    ];

    public function create(array $attributes)
    {
        if (!$this->role->isAdmin()) {
            session()->flash('error', 'Недосточно прав для внесения депозита');
            return false;
        }
        return parent::create($attributes);
    }


    public function makeDeposit(int $investorId, int $amount, string $description)
    {
        AccountOperation::create([
            'investor_id' => $investorId,
            'operation_name' => $description,
            'amount' => $amount
        ]);
        return true;
    }
}
