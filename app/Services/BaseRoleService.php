<?php

namespace App\Services;

use App\Services\RoleService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

abstract class BaseRoleService
{
    protected RoleService $role;
    protected ?Model $model = null;
    protected string $modelClass;
    protected array $publicColumns;

    public function __construct(bool $isInvestor = false)
    {
        $this->role = new RoleService($isInvestor);

        switch (static::class) {
            case "App\Services\UserService":
                $this->modelClass = '\App\Models\User';
                break;

            case "App\Services\InvestorService":
                $this->modelClass = '\App\Models\Investor';
                break;

            case "App\Services\CarService":
                $this->modelClass = '\App\Models\Car';
                break;

            case "App\Services\ResaleProjectService":
                $this->modelClass = '\App\Models\ResaleProject';
                break;

            case "App\Services\WorkingOrderService":
                $this->modelClass = '\App\Models\WorkingOrder';
                break;

            case "App\Services\FileService":
                $this->modelClass = '\App\Models\File';
                break;

            case "App\Services\AccountOperationService":
                $this->modelClass = '\App\Models\AccountOperation';
                break;
        };
    }

    public function getPulbicColumns(): array
    {
        return $this->publicColumns;
    }

    protected function findModel($id = null)
    {
        return call_user_func($this->modelClass."::findOrFail", $id);
    }

    public function setModelById(int $id)
    {
        $this->model = $this->findModel($id);
    }

    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    public function getEmptyProperies()
    {
        return array_fill_keys($this->publicColumns, '');
    }

    public function create(array $attributes)
    {
        return call_user_func($this->modelClass."::create", $attributes);
    }

    public function updateProperties(array $attributes, int $modelId = 0)
    {
        if ($modelId == 0) {
            return $this->model->update($attributes);
        } else {
            $model = $this->findModel($modelId);
            return $model->update($attributes);
        }
    }

    public function getProperties(int $modelId = 0): array
    {
        $retVal = [];
        $model = $modelId == 0 ? $this->model: $this->findModel($modelId);
        foreach ($this->publicColumns as $column) {
            $retVal[$column] = $model->$column;
        }
        return $retVal;
    }

    public function getPropertie(string $column, int $modelId = 0)
    {
        $model = $modelId == 0 ? $this->model: $this->findModel($modelId);
        return $model->$column;
    }

    public function getPropertiesForAll(): array
    {
        $retVal = [];
        $users = call_user_func($this->modelClass."::all");
        foreach ($users as $user) {
            foreach ($this->publicColumns as $column) {
                $retVal[$user->id][$column] = $user->$column;
            }
        }
        return $retVal;
    }

    public function changeProperty(string $property, string $newValue, int $modelId = 0)
    {
        if ($modelId != 0) {
            $ret = call_user_func($this->modelClass."::where", 'id', $modelId);
            $ret->update([$property => $newValue]);
        } else {
            $this->model->$property = $newValue;
            $this->model->save();
        }
    }
}
