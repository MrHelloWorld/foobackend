<?php

namespace App\Services;

use App\Models\Investor;
use App\Models\ResaleProject;
use App\Models\User;
use App\Models\WorkingOrder;

class StatisticsService
{
    public static function getResaleProjectsCount(bool $forCurrnetUser = true): int
    {
        if ($forCurrnetUser)
            return ResaleProject::count();
        else
            return ResaleProject::where('buyer_id', Auth()->user()->id)->count();
    }

    public static function getFinishedResaleProjectsCount(bool $forCurrnetUser = true): int
    {
        if ($forCurrnetUser)
            return ResaleProject::whereNotNull('seller_id')->count();
        else
            return ResaleProject::whereNotNull('seller_id')->where('buyer_id', Auth()->user()->id)->count();
    }

    public static function getWorkingOrdersCount(bool $forCurrnetUser = true): int
    {
        if ($forCurrnetUser)
            return WorkingOrder::count();
        else
            return WorkingOrder::where('created_user_id', Auth()->user()->id)
                ->where('type', '<>', WorkingOrder::ORDER_TYPE_SALE)
                ->count();
    }

    public static function getFinishedWorkingOrdersCount(bool $forCurrnetUser = true): int
    {
        if ($forCurrnetUser)
            return WorkingOrder::whereNotNull('end_datetime')->where('type', '<>', WorkingOrder::ORDER_TYPE_SALE)->count();
        else
            return WorkingOrder::whereNotNull('end_datetime')
                ->where('type', '<>', WorkingOrder::ORDER_TYPE_SALE)
                ->where('created_user_id', Auth()->user()->id)
                ->count();
    }

    public static function getUsersCount(): int
    {
        return User::count();
    }

    public static function getInvestorsCount(): int
    {
        return Investor::count();
    }
}
