<?php

namespace App\Http\Middleware;

use App\Models\Investor;
use Closure;
use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

class CheckAuthInvestorToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = PersonalAccessToken::findToken($request->bearerToken());

        if ($token) {
            $token->last_used_at = now();
            $token->save();
            $request->merge(['investor_id' => $token->tokenable_id]);
            $request->merge(['token_id' => $token->id]);
        } else {
            if ($request->expectsJson())
            //return redirect()->route('investor.login');
                return response()->json(['Message' => 'Unauthorized'], 401);
            else
                return redirect()->route('investor.login'); //response()->json(['Message' => 'Unauthorized'], 401);
        }
        return $next($request);
    }
}
