<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Routing\Controller;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\BaseRoleService;

class BaseApiController extends Controller
{
    protected $serviceClass;
    protected $requestClass;

    protected ?BaseRoleService $service = null;
    protected ?FormRequest $request = null;

    protected function init()
    {
        $this->service = $this->service != null ? $this->service : (new $this->serviceClass());
        $this->request = $this->request != null ? $this->request : app()->make($this->requestClass);
    }

    public function index()
    {
        $this->init();
        $props = $this->service->getPropertiesForAll();
        return response()->json(array_values($props), 200);
    }

    public function show(string $id)
    {
        $this->init();
        try {
            $props = $this->service->getProperties(intval($id));
        } catch (Exception $ex) {
            return response()->json(null, 404);
        }
        return response()->json($props, 200);
    }

    public function store()
    {
        $this->init();
        try {
            $user = $this->service->create($this->request->validated());
        } catch (Exception $ex) {
            return response()->json([
                'Message' => 'Unexepted error',
                'Code' => $ex->getCode()
            ], 422);
        }
        return response()->json($user, 200);
    }

    public function update(string $id)
    {
        $this->init();
        try {
            $this->service->updateProperties($this->request->validated(), $id);
            $newCar = $this->service->getProperties(intval($id));
            return response()->json($newCar, 200);
        } catch (ModelNotFoundException $ex) {
            $message = 'Record not found';
        } catch (Exception $ex) {
            $message = 'Unexepted error';
        }
        return response()->json(['Message' => $message], 422);
    }
}
