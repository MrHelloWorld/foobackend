<?php

namespace App\Http\Controllers\Api\v1;

class CarsController extends BaseApiController
{
    protected $serviceClass = "App\Services\CarService";
    protected $requestClass = "App\Http\Requests\CarRequest";

}
