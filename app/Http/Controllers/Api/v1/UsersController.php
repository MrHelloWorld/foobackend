<?php

namespace App\Http\Controllers\Api\v1;

class UsersController extends BaseApiController
{
    protected $serviceClass = 'App\Services\UserService';
    protected $requestClass = 'App\Http\Requests\UserRequest';
}
