<?php

namespace App\Http\Controllers\Api\v1\Token;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class LoginController
{
    public function login(Request $request)
    {
        if ($request->has('email')) {
            //dd(User::where('email', $request->input('email'))->get());
            if (Auth::attempt($request->only('email', 'password'))) {

                $token = Auth::user()->createToken('token');
                Auth::logout();
                return response()->json([
                    'Message' => 'Login success',
                    'Token' => $token->plainTextToken
                    ]);
            }
            else {
                return response()->json(['Error' => "Email or password was incorrect"], 403);
            }
        }
    }

    public function logout()
    {
        $token = Auth::user()->currentAccessToken();
        $token->delete();
        return response()->json(['Message' => 'You were logged out']);
    }
}
