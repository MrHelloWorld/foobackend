<?php

namespace App\Http\Controllers\Api\v1;

class AccountOperationsController extends BaseApiController
{
    protected $serviceClass = 'App\Services\AccountOperationService';
    protected $requestClass = 'App\Http\Requests\AccountOperationRequest';

    public function update(string $id)
    {
        return response()->json(['Message' => 'Operation is not supported'], 422);
    }
}
