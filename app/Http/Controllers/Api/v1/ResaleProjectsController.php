<?php

namespace App\Http\Controllers\Api\v1;

class ResaleProjectsController extends BaseApiController
{
    protected $serviceClass = 'App\Services\ResaleProjectService';
    protected $requestClass = 'App\Http\Requests\ResaleProjectRequest';
}
