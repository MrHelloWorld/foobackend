<?php

namespace App\Http\Controllers\Api\v1;

class InvestorsController extends BaseApiController
{
    protected $serviceClass = "App\Services\InvestorService";
    protected $requestClass = "App\Http\Requests\InvestorRequest";

}
