<?php

namespace App\Http\Controllers\Api\v1;

class WorkingOrdersController extends BaseApiController
{
    protected $serviceClass = 'App\Services\WorkingOrderService';
    protected $requestClass = 'App\Http\Requests\WorkingOrderRequest';
}
