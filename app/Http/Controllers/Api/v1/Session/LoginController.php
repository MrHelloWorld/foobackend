<?php

namespace App\Http\Controllers\Api\v1\Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController
{
    public function login(Request $request)
    {
        if ($request->has('email')) {
            if (Auth::attempt($request->only('email', 'password'), true))
                return response()->json(['Message' => 'Login success']);
            else
                return response()->json(['Error' => 'Email or password are incorrect'], 403);
        }
    }

    public function logout()
    {
        Auth::logout();
        return response()->json(['Message' => 'You were logged out']);
    }
}
