<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use App\Services\CarService;

class CarsController extends Controller
{
    public function index(Request $request)
    {
        return view('cars');
    }

    public function details(CarRequest $request)
    {
        if ((int)$request->input('id') > 0) {
            return view('car-details');
        }
        return redirect()->route('cars.index');
    }

    public function add(CarRequest $request)
    {
        if ($request->method() == "GET")
            return view('add-car');

        $service = new CarService();
        $service->create($request->validated());

        return redirect('cars');
    }
}
