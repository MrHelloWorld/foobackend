<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        if ($request->has('email')) {
            $authData = $request->only('email', 'password');
            $remember = (bool)$request->remember;
            if (Auth::attempt($authData, $remember)) {
                return redirect()->to('/');
            } else {
                return redirect('login?')->with('status', 'Почта или пароль указаны неверно');
            }
        } else {
            return view('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function welcome(Request $request)
    {
        if (Auth::check()) {
            $html = 'Welcome, registerd user!';
        } else {
            $html = 'You need to login';
        }
        return view('welcome', ['inject' => 'Welcome']);
    }
}
