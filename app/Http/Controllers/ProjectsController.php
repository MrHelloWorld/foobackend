<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(Request $request)
    {
        return view('projects');
    }

    public function add(Request $request)
    {
        return view('add-project');
    }

    public function details($id)
    {
        if ($id)
            return view('project-details', ['id' => intval($id)]);
        else
            return view('projects');
    }
}
