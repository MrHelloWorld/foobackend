<?php

namespace App\Http\Controllers\Investor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Investor;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;

class LoginController
{

    public function login(Request $request)
    {
        if ($request->has(['email', 'password'])) {
            $investor = Investor::where('email', $request->input('email'))->first();
            if ( $investor && Hash::check($request->input('password'), $investor->password)) {
                $token = $investor->createToken($investor->email);
                return response()->json([
                    'Message' => 'Login for investor success',
                    'Token' => $token->plainTextToken
                    ]);
            }
            else {
                return response()->json(['Error' => 'Email or password for investor are incorrect'], 401);
            }
        } else if ($request->has('token')) {
            $token = PersonalAccessToken::findToken($request->input('token'));
            if ($token) {
                $token->touch();
                $investor = $token->tokenable()->first();
                $arr = array_merge($investor->toArray(), ['balance' => $investor->calcBalance()]);
                return response()->json($arr);
            } else {
                return response()->json(['Error' => 'Invalid token'], 401);
            }
        } else {
            return response()->json(['Error' => 'Require login data'], 401);
        }
    }

    public function logout(Request $request)
    {
        PersonalAccessToken::destroy($request->input('token_id'));
        return response()->json(['Message' => 'You were logged out']);
    }
}
