<?php

namespace App\Http\Controllers\Investor;

use App\Services\InvestorService;
use App\Services\ResaleProjectService;
use App\Services\UserService;
use Illuminate\Http\Request;

class UsersController // extends Controller
{
    public function index(Request $request)
    {
        $projects = (new UserService(true))->getPropertiesForAll();
        return response()->json($projects, 200);
    }

    public function show(Request $request, $id)
    {
        if ($id = intval($id)) {
            $user = (new UserService(true))->getProperties($id);
            if (count($user) > 0) {
                return response()->json($user);
            }
        }
        return response()->json(null, 404);
    }
}
