<?php

namespace App\Http\Controllers\Investor;

use App\Services\InvestorService;
use App\Services\ResaleProjectService;
use App\Services\CarService;
use Illuminate\Http\Request;

class CarsController // extends Controller
{
    public function show(Request $request, $id)
    {
        if ($id = intval($id)) {

            $car = (new CarService(true))->getCarIfItBelongsToInvestor($id, $request->input('investor_id'));
            if ($car) {
                return response()->json($car);
            }
        }
        return response()->json(null, 404);
    }
}
