<?php

namespace App\Http\Controllers\Investor;

use App\Services\InvestorService;
use App\Services\ResaleProjectService;
use App\Services\WorkingOrderService;
use Illuminate\Http\Request;

class ProjectsController// extends Controller
{
    public function index(Request $request)
    {
        $wos = new WorkingOrderService(true);
        $projects = (new InvestorService(true))->getResaleProjects($request->input('investor_id'));
        foreach ($projects as $key => $value) {
            $projects[$key]->orders = array_values($wos->getPropertiesForCar($value->car_id));
        }
        return response()->json($projects, 200);
    }

    public function show(Request $request, $id)
    {
        if ($id = intval($id)) {
            $project = (new ResaleProjectService(true))->getProperties($id);
            if ($project['investor_id'] != $request->input('investor_id')) {
                return response()->json(null, 404);
            }
            $project['orders'] = array_values((new WorkingOrderService(true))->getPropertiesForCar($project['car_id']));
            return response()->json($project);
        }
        return response()->json(null, 404);
    }
}
