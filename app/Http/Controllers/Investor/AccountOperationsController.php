<?php

namespace App\Http\Controllers\Investor;

use App\Services\InvestorService;
use App\Services\ResaleProjectService;
use App\Services\CarService;
use App\Services\AccountOperationService;
use Illuminate\Http\Request;

class AccountOperationsController // extends Controller
{
    public function index(Request $request)
    {
        $operations = (new InvestorService(true))->getAccountOperations($request->input('investor_id'));
        if ($operations) {
            return response()->json($operations);
        }
        return response()->json(null, 404);
    }
}
