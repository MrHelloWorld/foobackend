<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use App\Helpers\FileStorage;

class FileController extends Controller
{
    public function add(Request $request)
    {
        $storageType = $request->input('storageType');
        $fileStatus = $request->input('fileType');
        $id = $request->input('destId') == null? 0: $request->input('destId');

        $storage = new FileStorage($storageType);
        if ($fileStatus == File::FILE_STATUS_UPLOADED) {
            $storage->addFileFromRequest($id);
        } elseif ($fileStatus == File::FILE_STATUS_WAITING) {
            $storage->addTempFileFromRequest();
        }
        return view('controllers.file.view-all-files', [
            'storageType' => $storageType,
            'storageFilesStatus'=> $fileStatus,
            'id' => $id
        ]);
    }

    public function delete(Request $request)
    {
        $fileName = $request->input('fileName');
        $storageType = $request->input('storageType');
        $fileStatus = $request->input('fileType');
        $id = $request->input('destId') == null? 0: $request->input('destId');
        FileStorage::deleteFile($fileName);

        return view('controllers.file.view-all-files', [
            'storageType' => $storageType,
            'storageFilesStatus'=> $fileStatus,
            'id' => $id
        ]);
    }

}
