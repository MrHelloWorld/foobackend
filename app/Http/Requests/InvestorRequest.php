<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class InvestorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return Auth::check();
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator): void
    {
        //dd('Not validated');
        parent::failedValidation($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allRules = [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'created_user_id' => Rule::exists('users', 'id'),
            'login' => 'required',
            'password' => 'required',
        ];
        $uniqRule = Rule::unique('investors', 'login')->ignore(intval($this->route('investor')));
        //$existsRule = Rule::exists('users', 'id');

        switch ($this->getMethod()) {
            case "GET":
                return [];

            case "POST":
                return ['login' => $uniqRule] + $allRules;

            case "PUT":
                return [
                    'login' => ['required', $uniqRule],
                    ] + $allRules;

            case "PATCH":
                $rules = [
                    'name' => '',
                    'surname' => '',
                    'email' => 'email',
                    'phone' => '',
                    'login' => $uniqRule,
                    'password' => ''];
                return $rules;
        }
        return [];
    }
}
