<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Services\WorkingOrderService;

class WorkingOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator): void
    {
        //dd('Not validated');
        parent::failedValidation($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ifProjectChanges = function ($attribute, $value, $fail) {
            $service = new WorkingOrderService();
            try {
                $id = intval(array_values($this->route()->parameters())[0]);
                $projectId = $service->getPropertie('resale_project_id', $id);
                if ($attribute == 'resale_project_id' && $value != $projectId) {
                    return $fail('Cant change project for order.');
                }
            } catch (Exception $exc) {
                return $fail('Cant get project for order');
            }
        };

        $allRules = [
            'resale_project_id' => ['required', Rule::exists('resale_projects', 'id')],
            'type' => ['required', 'integer', 'min:0', 'max:1'],
            'description' => ['required'],
            'price' => ['required', 'integer', "min:0"],
            //'created_user_id' => ['required', Rule::exists('users', 'id')],
            'start_datetime' => 'required|date_format:Y-m-d H:i:s',
            //'end_datetime' => 'required|date_format:Y-m-d H:i:s',
        ];
        //$uniqRule = Rule::unique('investors', 'login')->ignore(intval($this->route('investor')));
        //$existsRule = Rule::exists('users', 'id');
        switch ($this->getMethod()) {
            case "GET":
                return [];

            case "POST":
                return $allRules;

            case "PUT":
                return [
                    'resale_project_id' => $ifProjectChanges,
                    'end_datetime' => 'date_format:Y-m-d H:i:s', //If not in PUT request set to NULL???
                    ] + $allRules;

            case "PATCH":
                $rules = [
                    'resale_project_id' => $ifProjectChanges,
                    'type' => ['required', 'integer', 'min:0', 'max:1'],
                    'description' => ['required'],
                    'price' => ['integer', "min:0"],
                    'start_datetime' => 'date_format:Y-m-d H:i:s',
                    'end_datetime' => 'date_format:Y-m-d H:i:s'
                    ];
                return $rules;
        }
        return [];
    }
}
