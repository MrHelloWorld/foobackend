<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class AccountOperationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator): void
    {
        //dd('Not validated');
        parent::failedValidation($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allRules = [
            'operation_name' => 'required',
            'amount' => 'required|integer',
            'investor_id' => ['required', Rule::exists('investors', 'id')]
        ];
        //$uniqRule = Rule::unique('investors', 'login')->ignore(intval($this->route('investor')));
        //$existsRule = Rule::exists('users', 'id');
        //dd($this->all());
        switch ($this->getMethod()) {
            case "GET":
                return [];

            case "POST":
                return $allRules;

            case "PUT":
            case "PATCH":
                return [];
        }
        return [];
    }
}
