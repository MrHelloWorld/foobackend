<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator): void
    {
        //dd('Not validated');
        parent::failedValidation($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allRules = [
            'model' => 'required',
            'year' => 'required|integer|min:1900|max:2100',
            'color' => 'required',
            'buy_price' => 'integer|min:0',
            'type' => '',
            'reg_number' => '',
            'vin' => '',
            'kuzov_number' => '',
            'rama_number' => '',
            'power_horses' => '',
            'power_kvt' => '',
            'mass' => '',
            //'created_user_id' => Rule::exists('users', 'id')
        ];
        //$uniqRule = Rule::unique('investors', 'login')->ignore(intval($this->route('investor')));
        //$existsRule = Rule::exists('users', 'id');

        switch ($this->getMethod()) {
            case "GET":
                return [];

            case "POST":
                return $allRules;

            case "PUT":
                return $allRules + ['created_user_id' => Rule::exists('users', 'id')];

            case "PATCH":
                $rules = [
                    'model' => '',
                    'year' => '',
                    'color' => '',
                    'phone' => '',
                    'buy_price' => '',
                    'created_user_id' => Rule::exists('users', 'id')] + $allRules;
                return $rules;
        }
        return [];
    }
}
