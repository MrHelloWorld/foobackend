<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Services\ResaleProjectService;
use Exception;

class ResaleProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator): void
    {
        //dd('Not validated');
        parent::failedValidation($validator);
    }

    public function rules()
    {
        $ifCarChanges = function ($attribute, $value, $fail) {
            $service = new ResaleProjectService();
            $id = intval(array_values($this->route()->parameters())[0]);
            try {
                $carId = $service->getCarId($id);
            } catch (\Exception $exc) {
                return $fail("Cant find the car for this project");
            }
            if ($attribute == 'car_id' && $value != $carId) {
                return $fail("Cant change car in project. Delete project and recreate it with new car.");
            }
        };
        $ifBuyerChanges = function ($attribute, $value, $fail) {
            $service = new ResaleProjectService();
            $id = intval(array_values($this->route()->parameters())[0]);
            try {
                $buyerId = $service->getPropertie('buyer_id',$id);
            } catch (\Exception $exc) {
                return $fail("Cant find the buyer for this project");
            }
            if ($attribute == 'buyer_id' && $value != $buyerId) {
                return $fail("Cant change buyer in project. Delete project and recreate it with new buyer.");
            }
        };
        $allRules = [
            'description' => 'required',
            'investor_id' => ['required', Rule::exists('investors', 'id')],
            'car_id' => ['required', Rule::unique('resale_projects', 'car_id')],
            'buyer_id' => ['required', Rule::exists('users', 'id')],
            'buy_price' => 'required|integer|min:0',
            'buy_datetime' => 'required|date_format:Y-m-d H:i:s',
        ];
        switch ($this->getMethod()) {
            case "GET":
                return [];

            case "POST":
                return $allRules;

            case "PUT":
                return ['car_id' => $ifCarChanges,
                        'buyer_id' => $ifBuyerChanges] + $allRules;

            case "PATCH":
                $rules = [
                    'description' => '',
                    'investor_id' => [Rule::exists('investors', 'id')],
                    'car_id' => $ifCarChanges,
                    'buyer_id' => $ifBuyerChanges,
                    'buy_price' => '',
                    'buy_datetime' => ''];
                return $rules;
        }
        return [];
    }
}
