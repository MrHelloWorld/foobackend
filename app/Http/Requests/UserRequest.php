<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return Auth::check();
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator): void
    {
        //dd('Not validated');
        parent::failedValidation($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allRules = [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:App\Models\User,email',
            'position' => 'required',
            'role' => 'required|integer|min:1|max:3',
            'password' => 'required',
        ];
        $uniqRule = Rule::unique('users', 'email')->ignore(intval($this->route('user')));

        switch ($this->getMethod()) {
            case "GET":
                return [];

            case "POST":
                return $allRules;

            case "PUT":
                return ['email' => [
                        'required',
                        'email',
                        $uniqRule]
                    ] + $allRules;

            case "PATCH":
                $rules = [
                    'name' => '',
                    'surname' => '',
                    'email' => ['email',$uniqRule],
                    'position' => '',
                    'role' => 'integer|min:1|max:3',
                    'password' => ''];
                return $rules;
        }
        return [];
    }
}
