<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditElement extends Component
{
    public string $emitEvent;
    public ?string $value;
    public ?string $initValue;
    public string $uid;
    public string $validateRule = '';

    protected $listeners =[
        'updateEditElement' => '$refresh'
    ];

    public function mount()
    {
        $this->initValue = $this->value;
        $this->uid = 'd'.rand(10000, 99999);
    }

    public function render()
    {
        return view('livewire.edit-element');
    }

    public function acceptClick()
    {
        $this->validate(['value' => $this->validateRule]);

        $this->initValue = $this->value;
        $this->emitUp($this->emitEvent, $this->value);
    }
}
