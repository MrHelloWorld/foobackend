<?php

namespace App\Http\Livewire\WorkingOrders;

use Livewire\Component;
use App\Models\WorkingOrder;
use App\Helpers\FileStorage;
use App\Models\ResaleProject;
use App\Services\InvestorService;
use App\Services\ResaleProjectService;
use App\Services\WorkingOrderService;

class AddForm extends Component
{
    public $startDateTime;
    public $price = 10;
    public $description = 'Выполнение работ по ремонту покрышек и всякой прочей хуйни';
    public $visible;
    public $resaleProjectId;
    public $investorBalance;

    protected $rules = [
        'startDateTime' => 'required',
        'price' => 'required|integer|min:0',
    ];

    protected $listeners = [
        'showAddOrderForm'
    ];

    public function showAddOrderForm(bool $visible)
    {
        $this->visible = $visible;
    }

    public function submit()
    {
        $this->validate();

        $service = new WorkingOrderService();

        $order = $service->create([
            'resale_project_id' => $this->resaleProjectId,
            'type' => WorkingOrder::ORDER_TYPE_WORK,
            'description' => $this->description,
            'price' => $this->price,
            'start_datetime' => $this->startDateTime,
        ]);

        if ($order == false) {
            return redirect()->route('cars.index');
        }
        $fs = new FileStorage(FileStorage::STORAGE_TYPE_WORKING_ORDER);
        $fs->acceptAllTempFiles($order->id);
        $this->emit('updateOrdersTable');
        $this->visible = false;
    }

    public function mount()
    {
        $this->startDateTime = now()->format("Y-m-d");
        $investorId = (new ResaleProjectService())->getPropertie('investor_id', $this->resaleProjectId);
        $this->investorBalance = (new InvestorService())->calcBalance($investorId);
    }

    public function render()
    {
        return view('livewire.working-orders.add-form');
    }
}
