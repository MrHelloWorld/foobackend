<?php

namespace App\Http\Livewire\WorkingOrders;

use Livewire\Component;
use App\Services\WorkingOrderService;

class EndForm extends Component 
{  
    public $endDatetime;
    public $price = 0;
    public $visible;
    public $orderId;
    
    protected $rules = [
        'endDatetime' => 'required',
        'price' => 'required|integer|min:0',
    ];
    
    protected $listeners = [
        'showEndOrderForm'
    ];
    
    public function showEndOrderForm(int $orederId)
    {
        $this->orderId = $orederId;
        $this->visible = true;
    }
    
    public function submit()
    {
        $this->validate();
        
        (new WorkingOrderService())->closeOrder($this->endDatetime, $this->price, $this->orderId);
                
        $this->emit('updateOrdersTable');
        $this->visible = false;
    }
    
    public function mount()
    {
        $this->endDatetime = now()->format("Y-m-d");
        $this->orderId = 0;
    }
    
    public function render() 
    {
        return view('livewire.working-orders.end-form');       
    }
}
