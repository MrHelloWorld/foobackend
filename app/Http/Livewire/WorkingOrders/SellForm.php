<?php

namespace App\Http\Livewire\WorkingOrders;

use Livewire\Component;
use App\Services\CarService;
use App\Helpers\FileStorage;

class SellForm extends Component 
{  
    public $resaleProjectId;
    public $visible = false;
    public $price;
    public $sellDatetime;
    public $description = 'Продажа';
            
    public $rules = [
        'price' => 'required|integer|min:0'
    ];
    
    protected $listeners = [
        'showSellCarForm'
    ];
    
    public function showSellCarForm($visible)
    {
        $this->visible = $visible;
    }
    
    public function submit()
    {
        $this->validate();
                
        $order = (new CarService())->sell($this->resaleProjectId, $this->description, $this->price, $this->sellDatetime);
        $fs = new FileStorage(FileStorage::STORAGE_TYPE_WORKING_ORDER);
        $fs->acceptAllTempFiles($order->id);
        redirect()->route('projects.index');
    }
    
    public function mount()
    {
        $this->sellDatetime = now()->format("Y-m-d");
    }
    
    public function render() 
    {
        return view('livewire.working-orders.sell-form');       
    }
}
