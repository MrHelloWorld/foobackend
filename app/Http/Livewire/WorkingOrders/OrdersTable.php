<?php

namespace App\Http\Livewire\WorkingOrders;

use Livewire\Component;
use App\Services\WorkingOrderService;
use App\Services\UserService;

class OrdersTable extends Component
{
    public int $carId;
    public array $orders;
    public int $deleteOrderId;
    public string $deleteOrderModalName = 'deleteOreder';

    public $listeners = [
        'updateOrdersTable' => '$refresh',
        'clickedYes'
    ];

    public function deleteOrder(int $orederId)
    {
        $this->deleteOrderId = $orederId;
        $this->emit('showModalDialog', $this->deleteOrderModalName);
    }

    public function showEndOrderForm(int $orederId)
    {
        $this->emit('showEndOrderForm', $orederId);
    }

    public function clickedYes($modal)
    {
        if ($modal == $this->deleteOrderModalName) {
            (new WorkingOrderService())->delete($this->deleteOrderId);
            $this->emit('hideModalDialog', $this->deleteOrderModalName);
        }
    }

    public function showFilesForm(int $orderId)
    {
        $this->emit('showFilesForm', $orderId);
    }

    public function mount()
    {
        //$this->orders = Car::find($this->carId)->resale_project->working_order;
    }

    public function render()
    {
        $this->orders = (new WorkingOrderService())->getPropertiesForCar($this->carId);
        foreach($this->orders as $order) {
            $this->orders[$order['id']]['createdUserProps'] = (new UserService())->getProperties($order['created_user_id']);
        }
        return view('livewire.working-orders.orders-table');
    }
}
