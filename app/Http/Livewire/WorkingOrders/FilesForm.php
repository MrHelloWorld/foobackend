<?php

namespace App\Http\Livewire\WorkingOrders;

use Livewire\Component;

class FilesForm extends Component 
{  
    public $orderId;
    public $visible;
    
    protected $listeners = [
        'showFilesForm'
    ];
    
    public function showFilesForm(int $orderId)
    {
        $this->visible = true;
        $this->orderId = $orderId;
    }
    
    public function hide()
    {
        $this->visible = false;
    }
    
    public function mount()
    {
        $this->orderId = 0;
    }
    
    public function render() 
    {
        return view('livewire.working-orders.files-form');       
    }
}
