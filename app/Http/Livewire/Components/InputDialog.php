<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;
use Illuminate\Support\Facades\Validator;

class InputDialog extends Component
{
    public bool $visible;
    public string $header;
    public string $inputCaption;
    public string $validationRule;
    public string $inputText="";
    public string $name;
    
    protected $listeners = [
        'showInputDialog',
        'hodeInputDialog'
    ];
    
    public function mount()
    {

    }

    public function showInputDialog(string $name)
    {
        if ($this->name == $name)
            $this->visible = true;
    }
    
    public function hideInputDialog(string $name)
    {
        if ($this->name == $name)
            $this->visible = false;        
    }
    
    public function clickedOk()
    {
        Validator::make([$this->inputText], [$this->validationRule])->validate();
        
        $this->emitUp('clickedOk', $this->name, $this->inputText);        
        $this->visible = false;
    }
    
    public function clickedCancel()
    {
        $this->visible = false;
    }
    
    public function render()
    {
        return view('livewire.components.input-dialog');
    }
    
    public function hide()
    {
        $this->visible = false;
    }
}
