<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class ModalDialog extends Component
{
    public const MODAL_DIALOG_OK = 1;
    public const MODAL_DIALOG_OK_CANCEL = 10;
    public const MODAL_DIALOG_YES_NO = 20;

    public $visible;
    public $body;
    public $header;
    public $name;
    public $type;

    protected $listeners = [
        'showModalDialog',
        'hideModalDialog',
    ];

    public function hide()
    {
        $this->visible = false;
    }
    
    public function showModalDialog(string $name)
    {
        $this->name == $name ? $this->visible = true: 0;        
    }

    public function hideModalDialog(string $name)
    {
        $this->name == $name ? $this->visible = false: 0;
    }

    public function clickedOk()
    {
        $this->emitUp('clickedOk', $this->name);
    }

    public function clickedYes()
    {
        $this->emitUp('clickedYes', $this->name);
    }

    public function clickedNo()
    {
        $this->emitUp('clickedNo', $this->name);
        $this->hide();
    }

    public function clickedCancel()
    {
        $this->emitUp('clickedCancel', $this->name);
    }

    public function mount()
    {

    }

    public function render()
    {
        return view('livewire.components.modal-dialog');
    }
}
