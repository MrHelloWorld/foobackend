<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use App\Services\UserService;

class AddForm extends Component
{
    public $name;
    public $surname;
    public $position;
    public $email;
    public $role;
    public $password;
    public $password_confirmation;

    public $showForm;

    protected $listeners = ['showAddForm' => 'show'];

    protected $rules = [
        'name' => 'required',
        'surname' => 'required',
        'email' => 'required|email',
        'position' => 'required|',
        'role' => 'required|integer|min:1|max:3',
        'password' => 'required|confirmed',
    ];

    public function show()
    {
        $this->showForm = true;
    }

    public function render()
    {
        $f = \Faker\Factory::create();
        $this->name = $f->firstName;
        $this->surname = $f->lastName;
        $this->password = '11';
        $this->password_confirmation = '11';
        $this->position = $f->word();
        $this->email = $f->email;
        $this->role = rand(1,3);

        return view('livewire.users.addForm');
    }

    public function submit()
    {
        $this->validate();

        $service = new UserService();
        $res = $service->create([
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'position' => $this->position,
            'role' => $this->role,
            'password' => $this->password,
        ]);
        if (!$res) {
            redirect()->route('users.index');
        }
        $this->emit('addedUser');
        $this->showForm = false;
    }
}
