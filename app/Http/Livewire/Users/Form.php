<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use App\Models\User;
use App\Services\UserService;

class Form extends Component
{
    public $showAddForm = false;
    public $usersProperties;

    protected $listeners = [
        'addedUser' => '$refresh',
        'editedUser' => '$refresh'
        ];

    public function showEditForm(int $userId)
    {
        $this->emit('showEditForm', $userId);
    }

    public function showAddForm()
    {
        $this->emit('showAddForm');
    }

    public function render()
    {
        $service = new UserService();
        
        $this->usersProperties = $service->getPropertiesForAll();
        return view('livewire.users.form');
    }
}
