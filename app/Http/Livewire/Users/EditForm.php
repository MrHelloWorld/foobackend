<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;

class EditForm extends Component
{
    public $password;
    public $password_confirmation;
    public array $userProperties;
    public bool $showForm;

    public int $selectedUserId;

    protected $listeners = ['showEditForm'];

    protected $rules = [
        'userProperties.name' => 'required',
        'userProperties.surname' => 'required',
        'userProperties.email' => 'required|email',
        'userProperties.position' => 'required|',
        'userProperties.role' => 'required|integer|min:1|max:3',
        'password' => 'confirmed',
    ];

    public function showEditForm(int $userId)
    {
        $service = new UserService();
        $this->selectedUserId = $userId;

        $this->userProperties = $service->getProperties($userId);
        $this->showForm = true;
    }

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function render()
    {
        $this->password = "";
        $this->password_confirmation = "";
        return view('livewire.users.editForm');
    }

    public function submit()
    {
        $this->validate();

        $service = new UserService();
        $service->updateProperties($this->userProperties, $this->selectedUserId);
        if ($this->password != "") {
            $service->changePassword($this->password, $this->selectedUserId);
        }
        $this->emit('editedUser');
        $this->showForm = false;
    }
}
