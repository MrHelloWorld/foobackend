<?php

namespace App\Http\Livewire\Investors;

use Livewire\Component;
use App\Models\Investor;
use App\Services\InvestorService;

class Form extends Component 
{  
    public $showAddForm = false;
    public $investorsData;

    public function showAddForm()
    {
        $this->emit('showAddForm');
    }

    public function render() 
    {
        $service = new InvestorService();
        
        $this->investorsData = $service->getPropertiesForAll();
        return view('livewire.investors.form');
    }
}
