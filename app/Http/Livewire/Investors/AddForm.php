<?php

namespace App\Http\Livewire\Investors;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Services\InvestorService;

class AddForm extends Component
{
    public $name;
    public $surname;
    public $email;
    public $phone;
    public $balance;
    public $login;
    public $password;
    public $password_confirmation;

    public $showForm;

    protected $listeners = ['showAddForm' => 'show'];

    protected $rules = [
        'name' => 'required',
        'surname' => 'required',
        'email' => 'required|email',
        'balance' => 'required|integer|min:0',
        'login' => 'required',
        'password' => 'required|confirmed',
    ];

    public function show()
    {
        $this->showForm = true;
    }

    public function render()
    {
        $f = \Faker\Factory::create();
        $this->login = $f->userName;
        $this->password = '11';
        $this->password_confirmation = '11';
        $this->name = $f->firstName;
        $this->surname = $f->lastName;
        $this->email = $f->email;
        $this->phone = $f->phoneNumber;
        $this->balance = rand(1000000,10000000000);

        return view('livewire.investors.add-form');
    }

    public function submit()
    {
        $this->validate();

        $service = new InvestorService();
        $res = $service->create([
            'login' => $this->login,
            'password' => $this->password,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'phone' => $this->phone,
        ], $this->balance);
        if ($res == true)
            session()->flash('info', 'Инвестор создан успешно');
        return redirect()->route('investors.index');
    }
}
