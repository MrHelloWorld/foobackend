<?php

namespace App\Http\Livewire\Investors;

use Livewire\Component;
use Illuminate\Support\Facades\Validator;
use App\Services\InvestorService;
use App\Services\UserService;

class Card extends Component
{
    public string $depositDialogValidatioRule = 'int|min:0';
    public int $selected = -1;
    public int $investorId;
    public array $investorProperties;
    public array $userProperties;
    public $accountOperations = [];
    public $resaleProjects = [];
    public string $depositDialogName;
    public string $withdrawDialogName;

    protected $listeners = [
        'clickedOk'
    ];

    public function showDepositDialog()
    {
        $this->emit('showInputDialog', $this->depositDialogName);
    }

    public function showWithdrawDialog()
    {
        $this->emit('showInputDialog', $this->withdrawDialogName);
    }

    public function clickedOk(string $dialogName, string $value)
    {
        $service = new InvestorService();

        if ($dialogName == $this->depositDialogName) {
            Validator::make([$value], [$this->depositDialogValidatioRule])->validate();

            $service->makeDeposit($value, "Внесение средств", $this->investorId);
        } else
        if ($dialogName == $this->withdrawDialogName) {
            Validator::make([$value], [$this->depositDialogValidatioRule])->validate();

            $service->makeWithdraw($value, "Вывод средств",$this->investorId);
        }
        $this->select(1);
    }

    public function mount()
    {
        $userService = new UserService();

        $this->investorProperties = (new InvestorService())->getProperties($this->investorId);
        $this->userProperties = $userService->getProperties($this->investorProperties['created_user_id']);
        $this->depositDialogName = "depositDialog_$this->investorId";
        $this->withdrawDialogName = "withdrawDialog_$this->investorId";
    }

    public function select(int $select)
    {
        $service = new InvestorService();
        $service->setModelById($this->investorId);

        $this->selected = $select;
        switch ($select) {
            case 1:
                    $this->accountOperations = $service->getAccountOperations();
                    $this->resaleProjects = $service->getResaleProjects();
                    break;
            case 2:
                    $this->resaleProjects = $service->getResaleProjects();
                    break;
        }
    }

    public function render()
    {
        $service = new InvestorService();

        return view('livewire.investors.card', compact('service'));
    }
}
