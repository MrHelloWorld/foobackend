<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;
use App\Services\ResaleProjectService;
use App\Services\CarService;

class ProjectDetails extends Component
{
    public array $carProperties;
    public array $projectProperties;
    public int $projectId;
    public int $carId;
    public bool $addOrderFormVisible = true;
    public int $leftId;
    public int $rightId;

    protected $listeners = [
        'clickedYes'
    ];

    public function showAddOrderForm()
    {
        $this->emit('showAddOrderForm', true);
    }

    public function clickedYes($dialogName)
    {
        if ($dialogName == 'deleteProject')
        {
            (new ResaleProjectService())->delete($this->projectId);
            return redirect()->route('projects.index');
        }
    }

    public function showDeleteDialog()
    {
        $this->emit("showModalDialog", 'deleteProject');
    }

    public function mount()
    {
        try {
            //$this->projectId = intval(request()->input('id'));

            $projectService = new ResaleProjectService();
            $projectService->setModelById($this->projectId);
            $this->carId = $projectService->getCarId();

            $carService = new CarService();
            $carService->setModelById($this->carId);
            $this->carProperties = $carService->getProperties();
            $this->carProperties['isSold'] = $carService->isSold();

            $left = $projectService->getLeftId();
            $this->leftId = $left == null ? -1 : $left->id;
            $right = $projectService->getRightId();
            $this->rightId = $right == null ? -1 : $right->id;
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            return redirect()->route('projects.index');
        }
    }

    public function showSellCarForm()
    {
        $this->emit('showSellCarForm', true);
    }

    public function render()
    {
        return view('livewire.projects.project-details');
    }
}
;
