<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;
use App\Services\ResaleProjectService;
use App\Services\CarService;
use App\Services\InvestorService;

class Form extends Component
{
    public $deleteModalName = 'deleteProject';
    public $resaleProjects;
    public $showDetailsForm = false;
    public $deletedProjectId;

    protected $listeners =[
        'clickedYes'
    ];

    public function clickedYes(string $from)
    {
        if ($from == $this->deleteModalName) {
            (new ResaleProjectService())->delete($this->deletedProjectId);
            return redirect()->route('projects.index');
        }
    }

    public function mount()
    {
        $investorService = new InvestorService();
        $carService = new CarService();

        $this->resaleProjects = (new ResaleProjectService())->getPropertiesForAll();

        foreach ($this->resaleProjects as $project) {
            $investorService->setModelById($project['investor_id']);
            $this->resaleProjects[$project['id']]['investor'] = $investorService->getProperties();
            $this->resaleProjects[$project['id']]['investor']['balance'] = $investorService->calcBalance();
            $this->resaleProjects[$project['id']]['car'] = $carService->getProperties($project['car_id']);
        }
    }

    public function deleteProject(int $deletedId)
    {
        $this->emit('showModalDialog', $this->deleteModalName);
        $this->deletedProjectId = $deletedId;
    }

    public function render()
    {
        return view('livewire.projects.form');
    }
}
