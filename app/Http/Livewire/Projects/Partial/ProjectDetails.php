<?php

namespace App\Http\Livewire\Projects\Partial;

use Livewire\Component;
use App\Services\ResaleProjectService;
use App\Services\UserService;
use App\Services\InvestorService;
use Illuminate\Support\Facades\Log;

class ProjectDetails extends Component
{
    public array $resaleProject;
    public array $investor;
    public array $buyer;
    public array $seller;
    public array $car;
    public $projectId;

    protected $listeners = [
        'updateResaleProjectDetails',
    ];

    protected function updateData(int $pId)
    {
        $this->projectId = $pId;
        $pService = new ResaleProjectService();
        $iService = new InvestorService();
        $uService = new UserService();

        $pService->setModelById($pId);
        $this->resaleProject = $pService->getProperties();
        $this->resaleProject['profit'] = $pService->calcProfit();

        $iService->setModelById($this->resaleProject['investor_id']);
        $this->investor = $iService->getProperties();
        $this->investor['balance'] = $iService->calcBalance();
        $this->buyer = $uService->getProperties($this->resaleProject['buyer_id']);
        if ($this->resaleProject['seller_id']) {
            $this->seller = $uService->getProperties($this->resaleProject['seller_id']);
        } else {
            $this->seller = $uService->getEmptyProperies();
        }
    }

    public function updateResaleProjectDetails($pId)
    {
        $this->updateData($pId);
    }

    public function render()
    {
        $this->updateData($this->projectId);
        return view('livewire.projects.partial.project-details');
    }
}
