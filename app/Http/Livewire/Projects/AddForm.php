<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;
use App\Helpers\FileStorage;
use App\Services\InvestorService;
use App\Services\CarService;
use App\Services\ResaleProjectService;
use DateTime;

class AddForm extends Component
{
    public array $investorsData;
    public int $selectedInvestorIndex;
    public array $carsData;
    public int $selectedCarIndex;
    public int $price;
    public string $buyDate;
    public string $description;

    public function render()
    {
        return view('livewire.projects.add-form');
    }

    public function carChange()
    {
        if (count($this->carsData) > 0) {
            $this->price = $this->carsData[$this->selectedCarIndex]['buy_price'];
        }
    }

    public function mount()
    {
        $investorService = new InvestorService();

        $this->description = 'Описание проекта';
        $this->investorsData = $investorService->getPropertiesForAll();
        if (count($this->investorsData) == null) {
            session()->flash('error', "Нет инвесторов");
            return redirect()->route('projects.index');
        }
        foreach ($this->investorsData as $key => $investor) {
            $this->investorsData[$investor['id']]['balance'] = $investorService->calcBalance($investor['id']);
        }
        $this->carsData = (new CarService())->getCarPropertiesNotInProjects();
        if (count($this->carsData) == null) {
            session()->flash('error', "Нет автомобилей для выкупа");
            return redirect()->route('projects.index');
        }
        $this->selectedCarIndex = reset($this->carsData)['id'];
        $this->selectedInvestorIndex = reset($this->investorsData)['id'];
        $this->price = reset($this->carsData)['buy_price'];

        $dt = new DateTime();
        $this->buyDate = $dt->format("Y-m-d");
    }

    public function submit()
    {
        $dt = DateTime::createFromFormat("Y-m-d", $this->buyDate);
        if ($dt == false)
            return;

        $rp = (new ResaleProjectService())->create([
            'description' => $this->description,
            'investor_id' => $this->investorsData[$this->selectedInvestorIndex]['id'],
            'car_id' => $this->carsData[$this->selectedCarIndex]['id'],
            'buy_price' => $this->price,
            'buy_datetime' => $this->buyDate,
        ]);
        if ($rp) {
            $storage = new FileStorage(FileStorage::STORAGE_TYPE_RESALE_PROJECT);
            $storage->acceptAllTempFiles($rp->id);
        }

        return redirect()->route('projects.index');
    }
}
