<?php

namespace App\Http\Livewire\Cars;

use Livewire\Component;
use App\Services\CarService;
use App\Services\UserService;
use App\Services\ResaleProjectService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class Form extends Component
{
    public array $carsProperties;
    public string $deleteCarModalName = "deleteCar";
    public string $createProjectModalName = "createProject";
    public int $deletedCarId = 0;

    protected $listeners = [
        'clickedYes',
        'refreshCarsForm' => '$refresh'
    ];

    public function showCarDetails(int $carId)
    {
        $this->emit('showCarDetailsForm', $carId);
    }

    public function render()
    {
        $carService = new CarService();
        $userService = new UserService();
        $rpService = new ResaleProjectService();
        $users = [];

        $this->carsProperties = $carService->getPropertiesForAll();
        foreach ($this->carsProperties as $car) {
            $curId = $car['id'];
            $creatorId = $car['created_user_id'];

            $users[$creatorId] = Cache::remember("users:$creatorId", 1, function () use ($userService, $creatorId){
                return $userService->getProperties($creatorId);
            });
            $this->carsProperties[$curId]['creator_name'] = $users[$creatorId]['name'];
            $this->carsProperties[$curId]['creator_surname'] = $users[$creatorId]['surname'];
            $this->carsProperties[$curId]['isSold'] = false;
            $project = $rpService->getPropertiesByCarId($curId);
            if (count($project) > 0) {
                $this->carsProperties[$curId]['buy_datetime'] = $project['buy_datetime'];
                $this->carsProperties[$curId]['project_id'] = $project['id'];
                $users[$project['buyer_id']] = Cache::remember("users:".$project['buyer_id'], 1, function () use ($userService, $project){
                    return $userService->getProperties($project['buyer_id']);
                });
                $this->carsProperties[$curId]['buyer_name'] = $users[$project['buyer_id']]['name'];
                $this->carsProperties[$curId]['buyer_surname'] = $users[$project['buyer_id']]['surname'];

                if ($project['seller_id'] > 0) {
                    $this->carsProperties[$curId]['sell_datetime'] = $project['sell_datetime'];
                    $users[$project['seller_id']] = Cache::remember('users:'.$project['seller_id'], 1, function () use ($userService, $project) {
                        return $userService->getProperties($project['seller_id']);
                    });
                    $this->carsProperties[$curId]['seller_name'] = $users[$project['seller_id']]['name'];
                    $this->carsProperties[$curId]['seller_surname'] = $users[$project['seller_id']]['surname'];
                    $this->carsProperties[$curId]['isSold'] = true;
                } else {
                    $seller = array_fill_keys(['sell_datetime', 'seller_name', 'seller_surname'], '');
                    $this->carsProperties[$curId] = array_merge(
                        $this->carsProperties[$curId],
                        $seller
                    );
                }
            } else {
                $empty = array_fill_keys([
                    'buy_datetime',
                    'buyer_name',
                    'buyer_surname',
                    'sell_datetime',
                    'seller_name',
                    'seller_surname'], '');
                $this->carsProperties[$curId] = array_merge($this->carsProperties[$curId], $empty);
            }
        }
        return view('livewire.cars.cars-form');
    }

    public function deleteCar(int $carId)
    {
        $this->deletedCarId = $carId;
        $this->emit('showModalDialog', $this->deleteCarModalName);
    }

    public function addProject()
    {
        $this->emit('showModalDialog', $this->createProjectModalName);
    }

    public function clickedYes(string $dialogName)
    {
        if ($dialogName == $this->deleteCarModalName) {
            (new CarService())->delete($this->deletedCarId);
            $this->emit('hideModalDialog', $this->deleteCarModalName);
        } else
        if ($dialogName == $this->createProjectModalName) {
            return redirect()->route('projects.add');
        }
    }
}
