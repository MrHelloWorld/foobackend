<?php

namespace App\Http\Livewire\Cars\Partial;

use Livewire\Component;
use App\Services\CarService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class CarDetails extends Component
{

    public $carId;
    public array $car = [];
    public $rules = [
        'model' => 'required',
        'year' => 'required|integer|min:1900|max:2100',
        'color' => 'required',
        'buy_price' => 'integer|min:0',
    ];
    protected $listeners = [
        'modelWasChanged',
        'yearWasChanged',
        'typeWasChanged',
        'colorWasChanged',
        'reg_numberWasChanged',
        'vinWasChanged',
        'kuzov_numberWasChanged',
        'rama_numberWasChanged',
        'power_horsesWasChanged',
        'power_kvtWasChanged',
        'massWasChanged',
        'buy_priceWasChanged',
        'updateCarDetailsForm'
    ];

    public function updateCarDetailsForm(int $carId)
    {
        $this->carId = $carId;
        $this->emit('updateEditElement');
        $service = new CarService();
        $this->car = $service->getProperties($this->carId);
    }

    public function render()
    {
        if ($this->carId == -1)
            $this->car = (new CarService())->getEmptyProperies();
        return view('livewire.cars.partial.car-details');
    }

    public function modelWasChanged(string $new)
    {
        $validator = Validator::make([$new], [$this->rules['model']]);
        if (!$validator->fails()) {
            (new CarService())->changeProperty('model', $new, $this->carId);
        }
    }

    public function yearWasChanged(string $new)
    {
        $validator = Validator::make([$new], [$this->rules['year']]);
        if (!$validator->fails()) {
            (new CarService())->changeProperty('year', $new, $this->carId);
        }
    }

    public function typeWasChanged(string $new)
    {
        (new CarService())->changeProperty('type', $new, $this->carId);
    }

    public function colorWasChanged(string $new)
    {
        $validator = Validator::make([$new], [$this->rules['color']]);
        if (!$validator->fails()) {
            (new CarService())->changeProperty('color', $new, $this->carId);
        }
    }

    public function reg_numberWasChanged(string $new)
    {
        (new CarService())->changeProperty('reg_number', $new, $this->carId);
    }

    public function vinWasChanged(string $new)
    {
        (new CarService())->changeProperty('vin', $new, $this->carId);
    }

    public function kuzov_numberWasChanged(string $new)
    {
        (new CarService())->changeProperty('kuzov_number', $new, $this->carId);
    }

    public function rama_numberWasChanged(string $new)
    {
        (new CarService())->changeProperty('rama_number', $new, $this->carId);
    }

    public function power_horsesWasChanged(string $new)
    {
        (new CarService())->changeProperty('power_horses', $new, $this->carId);
    }

    public function power_kvtWasChanged(string $new)
    {
        (new CarService())->changeProperty('power_kvt', $new, $this->carId);
    }

    public function massWasChanged(string $new)
    {
        (new CarService())->changeProperty('mass', $new, $this->carId);
    }

    public function buy_priceWasChanged(string $new)
    {
        $validator = Validator::make([$new], [$this->rules['buy_price']]);
        if (!$validator->fails()) {
            (new CarService())->changeProperty('buy_price', $new, $this->carId);
        }
    }
}
