<?php

namespace App\Http\Livewire\Cars;

use Livewire\Component;

class CarsDetails extends Component
{
    public int $carId = -1;
    public $visible = false;

    protected $listeners = [
        'showCarDetailsForm'
    ];

    public function showCarDetailsForm(int $carId)
    {
        $this->visible = true;
        $this->emit('updateCarDetailsForm', $carId);
    }

    public function hide()
    {
        $this->visible = false;
        $this->emit('refreshCarsForm');
    }

    public function render()
    {
        return view('livewire.cars.cars-details');
    }
}
