<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\InvestorsController;
use App\Http\Controllers\CarsController;
use App\Http\Controllers\ProjectsController;
use App\Http\Controllers\FileController;
use Illuminate\Cache\RedisStore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::middleware(['guest'])->group(function() {
    Route::match(['get', 'post'], 'login', [LoginController::class, 'login'])->name('login');
});

Route::middleware(['auth'])->group(function () {
    Route::post('/files/add', [FileController::class, 'add'])->name('addFile');
    Route::post('/files/delete', [FileController::class, 'delete'])->name('deleteFile');
    Route::get('/', [LoginController::class, 'welcome']);
    Route::get('/investors', [InvestorsController::class, 'index'])->name('investors.index');
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::get('/cars', [CarsController::class, 'index'])->name('cars.index');
    Route::match(['get', 'post'], '/cars/add', [CarsController::class, 'add'])->name('cars.add');
    Route::get('/projects/', [ProjectsController::class, 'index'])->name('projects.index');
    Route::get('/projects/add', [ProjectsController::class, 'add'])->name('projects.add');
    Route::get('/projects/{id}', [ProjectsController::class, 'details'])->name('projects.details');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
});
