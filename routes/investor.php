<?php

use App\Http\Controllers\Investor\ProjectsController;
use App\Http\Controllers\Investor\CarsController;
use App\Http\Controllers\Investor\UsersController;
use App\Http\Controllers\Investor\LoginController;
use App\Http\Controllers\Investor\AccountOperationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return view("investor.index");
});
Route::get('/account', function () {
    return view("investor.account");
});

Route::get('login', function () {
    return view("investor.login");
})->name('investor.login');

Route::post('logout', [LoginController::class, 'logout']);

// Route::get('/projects', function () {
//     return view("investor.projects");
// });

Route::prefix('api')->middleware('auth.investor')->group(function () {
    Route::get('projects', [ProjectsController::class, 'index']);
    Route::get('projects/{id}', [ProjectsController::class, 'show']);

    Route::get('users/{id}', [UsersController::class, 'show']);
    Route::get('users', [UsersController::class, 'index']);

    Route::get('cars/{id}', [CarsController::class, 'show']);

    Route::get('accountoperations', [AccountOperationsController::class, 'index']);

    Route::post('login', [LoginController::class, 'login'])->withoutMiddleware("auth.investor");
});

