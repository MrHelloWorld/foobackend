<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Json;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//session auth API
Route::middleware(['api-session','auth'])
    ->namespace('App\Http\Controllers\Api\v1')
    ->prefix('session/v1')
    ->group(function () {
        Route::apiResource('users', 'UsersController');
        Route::apiResource('investors', 'InvestorsController');
        Route::apiResource('cars', 'CarsController');
        Route::apiResource('accountopertaions', 'AccountOperationsController');
        Route::apiResource('resaleprojects', 'ResaleProjectsController');
        Route::apiResource('workingorders', 'WorkingOrdersController');
        Route::post('login', [App\Http\Controllers\Api\v1\Session\LoginController::class, 'login'])->withoutMiddleware('auth');
        Route::post('logout', [App\Http\Controllers\Api\v1\Session\LoginController::class, 'logout']);
    });


//Token auth API
Route::middleware(['api-token','auth:sanctum'])
    ->namespace('App\Http\Controllers\Api\v1')
    ->prefix('token/v1')
    ->group(function () {
        Route::apiResource('users', 'UsersController');
        Route::apiResource('investors', 'InvestorsController');
        Route::apiResource('cars', 'CarsController');
        Route::apiResource('accountopertaions', 'AccountOperationsController');
        Route::apiResource('resaleprojects', 'ResaleProjectsController');
        Route::apiResource('workingorders', 'WorkingOrdersController');
        Route::post('login', [App\Http\Controllers\Api\v1\Token\LoginController::class, 'login'])->withoutMiddleware('auth:sanctum');
        Route::post('logout', [App\Http\Controllers\Api\v1\Token\LoginController::class, 'logout']);
    });
