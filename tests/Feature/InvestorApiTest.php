<?php

namespace Tests\Feature;

use App\Models\AccountOperation;
use App\Models\Car;
use App\Models\Investor;
use App\Models\ResaleProject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\WorkingOrder;
use Doctrine\Inflector\Rules\Word;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class InvestorApiTest extends TestCase
{
    use RefreshDatabase;

    protected $path = 'investor/api/';
    protected $token;

    protected function pathTo(string $path)
    {
        return $this->path . $path;
    }

    protected function seedDb()
    {
        User::factory()->count(10)->create([
            'role' => User::USER_ROLE_ADMIN
        ]);

        Investor::factory()->create([
            'email' => '1@2.3',
        ])->makeDeposit(100)->makeDeposit(2000)->makeWithdraw(500);
        Investor::factory()->create();

        Car::factory()->count(10)->create();
        ResaleProject::factory()->create([
            'investor_id' => 1,
            'car_id' => 1,
            'buy_price' => Car::find(1)->buy_price
        ]);
        ResaleProject::factory()->create([
            'investor_id' => 2,
            'car_id' => 2,
            'buy_price' => Car::find(3)->buy_price
        ]);
        ResaleProject::factory()->create([
            'investor_id' => 1,
            'car_id' => 3,
            'buy_price' => Car::find(3)->buy_price
        ]);
        ResaleProject::factory()->create([
            'investor_id' => 2,
            'car_id' => 4,
            'buy_price' => Car::find(3)->buy_price
        ]);
        foreach ([1,2,3,4] as $value) {
            WorkingOrder::factory()->count(5)->create([
                'resale_project_id' => $value,
                'created_user_id' => 1
            ]);
        }

    }

    /** @test*/
    public function loginByEmail()
    {
        $this->seedDb();
        $this->token = $this->postJson($this->pathTo('login'), [
            'email' => '1@2.3',
            'password' => '1'
        ])->assertJsonFragment(['Message' => 'Login for investor success'])->json()['Token'];
    }

    /** @test */
    public function loginByToken()
    {
        $this->seedDb();
        $this->loginByEmail();
        $this->postJson($this->pathTo('login'), ['token' => $this->token])->assertJsonFragment(['balance' => 1600]);
    }

    /** @test */
    public function canGetInvestorsProjects()
    {
        $this->loginByEmail();
        $this->withHeader('Authorization', "Bearer $this->token")->getJson($this->pathTo('projects'))->assertJsonCount(2);
    }

    /** @test */
    public function canGetAllUsers()
    {
        $this->loginByEmail();

        $this->withHeader('Authorization', "Bearer $this->token")->getJson($this->pathTo('users'))->assertJsonCount(10);
    }

    /** @test */
    public function canGetUser()
    {
        $this->loginByEmail();

        $this->withHeader('Authorization', "Bearer $this->token")->getJson($this->pathTo('users/3'));
    }

    /** @test */
    public function canGetInvestorsCar()
    {
        $this->seedDb();
        $this->loginByEmail();

        $this->withHeader('Authorization', "Bearer $this->token")->get($this->pathTo('cars/1'))->assertJsonFragment(['id' => 1]);
        $this->withHeader('Authorization', "Bearer $this->token")->get($this->pathTo('cars/3'))->assertJsonFragment(['id' => 3]);
    }

    /** @test */
    public function cantGetCarNoBelongsToInvestor()
    {
        $this->seedDb();
        $this->loginByEmail();

        $this->withHeader('Authorization', "Bearer $this->token")->get($this->pathTo('cars/2'))->assertNotFound();
    }

    /** @test */
    public function getInvestorAccountOperations()
    {
        $this->seedDb();

        $this->loginByEmail();
        $this->withHeader('Authorization', "Bearer $this->token")->get($this->pathTo('accountoperations'))->assertJsonCount(3);
    }
}
