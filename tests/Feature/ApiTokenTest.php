<?php

namespace Tests\Feature;

use App\Models\AccountOperation;
use App\Models\Car;
use App\Models\Investor;
use App\Models\ResaleProject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\WorkingOrder;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Stmt\Foreach_;

class ApiTokenTest extends TestCase
{
    use RefreshDatabase;

    protected $path = 'api/token/v1/';

    protected function pathTo(string $path)
    {
        return $this->path . $path;
    }

    private function login()
    {
        User::factory()->create([
            'email' => '1@2.4',
            'password' => Hash::make('1'),
            'role' => User::USER_ROLE_ADMIN
        ]);

        $token = $this->postJson($this->pathTo('login'), [
            'email' => '1@2.4',
            'password' => '1'
        ])->assertJsonFragment(['Message' => 'Login success'])->json()['Token'];

        $this->withToken($token);

    }

    /** @test */
    public function get_wrong_login_if_password_missmatch()
    {
        $this->withoutExceptionHandling();

        User::factory(1)->create([
            'email' => '1@2.3',
            'password' => Hash::make('1')
        ]);
        $response = $this->postJson($this->pathTo('login'), [
            'email' => '1@2.3',
            'password' => '12'
        ]);

        $response->assertJsonFragment(['Error' => 'Email or password was incorrect']);
    }

    /** @test */
    public function can_login()
    {
        $this->login();
    }

    /** @test */
    public function need_login_to_get_data()
    {
        $this->getJson($this->pathTo("users"))->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->getJson($this->pathTo("investors"))->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->getJson($this->pathTo("cars"))->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->getJson($this->pathTo("accountopertaions"))->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->getJson($this->pathTo("resaleprojects"))->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->getJson($this->pathTo("workingorders"))->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->postJson($this->pathTo("logout"))->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    /** @test */
    public function can_get_data()
    {
        $this->login();

        Investor::factory(4)->create();
        Car::factory(5)->create();
        AccountOperation::factory(15)->create(['investor_id' => Investor::all('id')->random()]);
        ResaleProject::factory()->create([
            'investor_id' => Investor::all('id')->random(),
            'car_id' => Car::all('id')->random(),
            'buy_price' => 1111
        ]);
        WorkingOrder::factory(7)->create([
            'resale_project_id' => ResaleProject::all('id')->random()->id,
            'created_user_id' => 1
        ]);
        $this->getJson($this->pathTo("users"))->assertStatus(200);
        $this->getJson($this->pathTo("investors"))->assertJsonCount(4);
        $this->getJson($this->pathTo("cars"))->assertJsonCount(5);
        $this->getJson($this->pathTo("accountopertaions"))->assertJsonCount(15);
        $this->getJson($this->pathTo("resaleprojects"))->assertStatus(200);
        $this->getJson($this->pathTo("workingorders"))->assertStatus(200);
        $this->postJson($this->pathTo("logout"))->assertStatus(200);
    }
}
