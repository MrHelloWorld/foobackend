export function getHumanDate(datetime) {
    return datetime ? new Date(datetime).toLocaleDateString() : "";
}

export function getHumanDateTime(datetime) {
    return datetime ? new Date(datetime).toLocaleString() : "";
}
