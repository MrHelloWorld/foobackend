import axios from "axios";

export const carsMixin = {

    data: function () {
        return {
            carsData: {
                error: '',
                cars: [{}]
            }
        }
    },

    created: async function () {
        //await this.getAllUsers();
    },

    methods: {

        getCar: async function (id) {
            if (this.carsData.cars[id] != null)
                return this.carsData.cars[id];
            await axios.get(`/investor/api/cars/${id}`).then(response => {
                this.carsData.cars[id] = response.data;
                this.carsData.error = '';
            }).catch(error => {
                this.carsData.error = 'Error pending data';
            });
            return this.carsData.cars[id];
        }
    },
}

export default carsMixin;
