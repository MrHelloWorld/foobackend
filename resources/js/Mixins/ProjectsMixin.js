import axios from "axios";

const projectsMixin = {

    data: function () {
        return {
            projectsData: {
                error: '',
                projects: [{}]
            }
        }
    },

    created: async function () {
        //await this.getAllProjects();
    },

    methods: {
        getAllProjects: async function () {
            let resp;
            await axios.get('/investor/api/projects').then(response => {
                this.projectsData.projects = response.data;
                this.projectsData.projects.forEach(
                    (project) => project.orders.forEach(
                        order => order.type = order.type == 0 ? 'Работа' : 'Продажа'
                    )
                );
                this.projectsData.error = '';
            }).catch(error => {
                this.projectsData.projects = [];
                this.projectsData.error = 'Error pending data';
            });
        },
    },
}

export default projectsMixin;
