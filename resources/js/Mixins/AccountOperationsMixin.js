import axios from 'axios'

const accountOperationMixin = {

    data: () => {
        return {
            accountOperations: {}
        }
    },

    created: async function () {
        await this.getAccountOperations();
    },

    methods: {
        getAccountOperations: async function() {
            await axios.get('/investor/api/accountoperations')
                .then(response => {
                    this.accountOperations = response.data;
                })
                .catch(error => console.log(error));
        }
    }
}

export default accountOperationMixin;
