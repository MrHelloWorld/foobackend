import { mapState } from 'vuex'

const loginMixin = {

    computed: mapState({
        isLoggedIn: (state) => state.loginApi.isLoggedIn,
        getInvestor: function (state) {
            //console.log(state.loginApi);
            return state.loginApi.loginUser
        },
        getLoginToken: (state) => state.loginApi.token
    }),

    methods: {
        loginInvestorByToken: async function () {
            return await this.$store.dispatch('loginApi/loginInvestor');
        },

        logoutInvestor: function () {
            return this.$store.dispatch('loginApi/logoutInvestor');
        }
    }
}

export default loginMixin;
