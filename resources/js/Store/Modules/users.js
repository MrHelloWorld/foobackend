import axios from "axios"

export default {
    namespaced: true,

    state: () => ({
        users: {},
    }),

    getters: {},

    actions: {

        async getUser({ commit, state }, id) {
            if (state.users[id] != null) {
                return state.users[id];
            }
            await axios.get(`/investor/api/users/${id}`)
                .then(response => {
                    commit('setUser', { id, user: response.data });
                    return response.data;
                }).catch(error => {
                    console.log('Error');
                    console.dir(error);
                });
            return state.users[id];
        }
    },

    mutations: {
        setUser(state, { id, user }) {
            switch (user.role) {
                case 1:
                    user.role = 'Администратор';
                    break;
                case 2:
                    user.role = 'Выкупщик';
                    break;
                case 3:
                    user.role = 'Продавец';
                    break;
                default:
                    user.role = 'ХЗ'
                    break;
            }
            state.users[id] = user;
        }
    }
}
