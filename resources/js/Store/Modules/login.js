import axios from 'axios'

export default {

    namespaced: true,

    state: () => ({
        isLoggedIn: false,
        loginUser: {},
        token: ''
    }),

    getters: {},

    actions: {
        getInvestor: ({ state, }) => state.loginUser,

        loginInvestor: async function ({ commit, dispatch }, data = null) {

            let token = window.localStorage.getItem('investor-token');
            if (token) {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            }
            const { email, password } = data ? { email: data.email, password: data.password } : {};
            if (email != null && password != null) {
                const response = await dispatch('loginByEmail', { email, password });
                if (response.status === 200) {
                    window.localStorage.setItem('investor-token', response.data.Token);
                    commit('setLoginFlag');
                    commit('setToken', response.data.Token);
                    return true;
                }
            } else if (token !== null) {
                const response = await dispatch('loginByToken', token);
                if (response.status === 200) {
                    commit('setLoginFlag');
                    commit('setLoginUser', response.data);
                    return true;
                }
            }
            return false;
        },

        loginByEmail: async function ({ }, { email, password }) {
            let ret;
            await axios.post('/investor/api/login', {
                email: email,
                password: password
            })
                .then(function (resp) {
                    ret = resp;
                })
                .catch(function (error) {
                    ret = error.response;
                })
            return ret;
        },

        loginByToken: async function ({ }, token) {
            let ret;
            await axios.post('/investor/api/login', {
                token: token
            })
                .then(function (resp) {
                    ret = resp;
                })
                .catch(function (error) {
                    ret = error.response;
                })
            return ret;
        },

        logoutInvestor: async function ({ commit }) {
            commit('logoutUser')
            axios.post('/investor/logout', {});
            window.localStorage.removeItem('investor-token');
        }

    },

    mutations: {
        setLoginUser(state, user) {
            state.loginUser = user;
            state.isLoggedIn = true;
        },

        setLoginFlag(state) {
            state.isLoggedIn = true;
        },

        setToken(state, token) {
            state.token = token;
        },

        logoutUser(state) {
            state.loginUser = {};
            state.isLoggedIn = false;
        }
    }

}
