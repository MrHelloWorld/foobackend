import Vue from 'vue';
import Vuex from 'vuex';
import usersApi from './Modules/users'
import loginApi from './Modules/login'

Vue.use(Vuex)

export default new Vuex.Store({

    modules: {
        usersApi,
        loginApi
    }
});
