
import Vue from 'vue';

import IndexPage from './Pages/IndexPage.vue'
import AccountPage from './Pages/AccountPage'
import LoginPage from './Pages/LoginPage.vue'

import loginMixin from "./Mixins/LoginMixin.js"
import AppHeader from "./Components/TheAppHeader.vue";
import AppFooter from "./Components/TheAppFooter.vue";
import PageTitle from "./Components/ThePageTitle.vue";
import PageContent from "./Components/ThePageContent.vue";

import store from './Store/index'

Vue.mixin(loginMixin);
Vue.component('AppHeader', AppHeader);
Vue.component('AppFooter', AppFooter);
Vue.component('PageTitle', PageTitle);
Vue.component('PageContent', PageContent);

const vm = new Vue({
    el: '#app',

    components: { IndexPage, LoginPage, AccountPage },

    store,
});

