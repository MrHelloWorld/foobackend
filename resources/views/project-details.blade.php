<x-app-layout>
    <x-page-header>Подробнее о проекте</x-page-header>
    <livewire:projects.project-details :projectId="$id"/>
</x-app-layout>
