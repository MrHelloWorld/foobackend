<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="/js/my.js" defer></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script> --}}
</head>

<body class="antialiased">
    <div class="w-full min-h-screen bg-gray-100">
        <div id="app" class="container mx-auto border-l border-r">
            {{ $slot }}
        </div>
    </div>
</body>

</html>
