<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">-->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles

        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="/js/my.js" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            <!-- Page Heading -->
            <header class="bg-white shadow container mx-auto bg-gray-50 border-b">
                <div class="pt-6 px-2 sm:px-6 lg:px-8">
                    <div class="flex flex-col">
                        <div class="flex justify-between text-sm font-semibold border-b my-2">
                            <div class="px-2 tracking-tight">{{Auth::user()->name.' '.Auth::user()->surname.'. '.Auth::user()->position}}</div>
                            <div class=""></div>
                            <div class="text-right">
                                <form action="{{  route('logout')}}" method="post">
                                    @csrf
                                    <button class="text-blue-700 hover:underline">Выход</button>
                                </form>

                            </div>
                        </div>
                        <x-mainmenu>
                        </x-mainmenu>
                    </div>
                </div>
            </header>

            <!-- Page Content -->
            <main>
                <div class="container bg-white shadow mx-auto min-h-screen">
                    @if( session('error'))
                    <div class="text-red-600">
                        Error: {{session('error')}}
                    </div>
                    @endif
                    @if( session('info'))
                    <div class="text-green-600">
                        {{session('info')}}
                    </div>
                    @endif
                    {{ $slot }}
                </div>
            </main>
        </div>

        @stack('modals')

        @livewireScripts
    </body>
</html>
