<x-app-layout>
    <x-page-header>Проекты</x-page-header>
    <div class="py-12 px-8">
        <livewire:projects.form />
    </div>
</x-app-layout>
