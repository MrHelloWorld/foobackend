<div>
    <div class="flex px-5 pt-5">
        <div class="w-1/6 my-auto text-gray-600">Инвестор</div>
        <div class="p-3 w-2/6">
            {{ $investor['name'].' '.$investor['surname'] }}
        </div>
    </div>
    <div class="flex px-10 pt-3 text-sm">
        <div class="w-1/6 text-gray-500">Электронная почта</div>
        <div class="w-2/6 text-gray-700">{{ $investor['email'] }}</div>
        <div class="w-1/6 text-gray-500">Телефон</div>
        <div class="w-2/6 text-gray-700">{{ $investor['phone'] }}</div>
    </div>
    <div class="flex px-10 pt-3 pb-5 border-b text-sm">
        <div class="w-1/6 text-gray-500">Завершено/Проектов</div>
        <div class="w-2/6 text-gray-700">1/1</div>
        <div class="w-1/6 text-gray-500">Баланс</div>
        <div class="w-2/6 text-gray-700 font-bold">
            {{ $investor['balance']}} руб.
        </div>
    </div>
    <div class="flex px-5 pt-5">
        <div class="w-1/6 my-auto text-gray-600">Автомобиль</div>
        <div class="p-3 w-5/6">
            {{ $car['model'].', '.$car['year'].', '.$car['color'].' цвет'}}
        </div>
    </div>
    <div class="flex px-8 ">
        <div class="w-1/6 my-auto text-gray-600">Добавил:</div>
        <div class="p-3 w-2/6 text">
            {{ $buyer['name'].' '.$buyer['surname'].' '.App\Helpers\HelpTo::getHumanDateTime($car['created_at']) }}
        </div>
        <div class="w-1/6 my-auto text-gray-600">Цена</div>
        <div class="p-3 w-2/6 text font-bold my-auto">{{ $car['buy_price'] }} руб.</div>
    </div>
    <div class="flex px-8">
        <div class="w-1/6 my-auto text-gray-600">Купил:</div>
        <div class="p-3 w-2/6">
            {{ $buyer['name'].' '.$buyer['surname'].' '.App\Helpers\HelpTo::getHumanDate($resaleProject['buy_datetime'])}}
        </div>
        <div class="w-1/6 my-auto text-gray-600">Цена</div>
        <div class="p-3 w-2/6 text font-bold my-auto">{{ $resaleProject['buy_price'] }} руб.</div>
    </div>
    <div class="flex px-8 pb-3 border-b">
        <div class="w-1/6 my-auto text-gray-600">Продал:</div>
        <div class="p-3 w-2/6 text">
            {{ $seller['name'].' '.$seller['surname'].' '.App\Helpers\HelpTo::getHumanDate($resaleProject['sell_datetime'])}}
        </div>
        <div class="w-1/6 my-auto text-gray-600">Цена</div>
        <div class="p-3 w-2/6 text font-bold my-auto">{{ $resaleProject['sell_price'] }} руб.</div>
    </div>

    <div class="flex px-5 py-5 border-b">
        <div class="w-1/6 my-auto text-gray-600">Прибыль</div>
        <div class="p-3 w-5/6 text font-bold text-green-700"> {{ $resaleProject['profit'] }} руб.</div>
    </div>

    <div class="flex px-5 py-5 border-b">
        <div class="w-1/6 my-auto">
            <label class="text-gray-600">Описание</label>
        </div>
        <textarea class="form-input p-3 w-5/6" rows="2">{{ $resaleProject['description'] }}</textarea>
    </div>
    
    <div class="flex px-5 py-5 border-b">
        <div class="w-1/6 my-auto text-gray-600">Файлы</div>
        <div class="w-5/6 font-bold">
            <x-file-storage-view :storageType="\App\Models\File::FILE_STATUS_UPLOADED"
                                 :storageFilesStatus="\App\Models\File::FILE_TYPE_RESALE_PROJECT"
                                 :id="$projectId"
                                 addButton="Добавить">
            </x-file-storage-view>
        </div>
    </div>
</div>    