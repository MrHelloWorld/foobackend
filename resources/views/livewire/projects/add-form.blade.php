<x-forms.form-frame>
    <x-forms.form-caption mainLabel="Введите данные" secondLabel="для покупки авто" />
    <form wire:submit.prevent="submit">
        <div class="flex px-5 pt-6">
            <div class="w-1/6 my-auto">
                <label class="text-gray-600">Инвестор</label>
            </div>
            <select class="form-input border p-3 w-2/6" wire:model.lazy="selectedInvestorIndex">
                @foreach ($investorsData as $key => $investor)
                    <option value="{{ $key }}">{{ $investor['name'] . ' ' . $investor['surname'] }} </option>
                @endforeach
            </select>
        </div>
        <div class="flex px-10 pt-3 text-sm">
            <div class="w-1/6 text-gray-500">Электронная почта</div>
            <div class="w-2/6 text-gray-700">{{ $investorsData[$selectedInvestorIndex]['email'] }}</div>
            <div class="w-1/6 text-gray-500">Телефон</div>
            <div class="w-2/6 text-gray-700">{{ $investorsData[$selectedInvestorIndex]['phone'] }}</div>
        </div>
        <div class="flex px-10 pt-3 pb-5 border-b text-sm">
            <div class="w-1/6 text-gray-500">Завершено/Проектов</div>
            <div class="w-2/6 text-gray-700">1/1</div>
            <div class="w-1/6 text-gray-500">Баланс</div>
            <div class="w-2/6 text-gray-700 font-bold">{{ $investorsData[$selectedInvestorIndex]['balance'] }} руб.
            </div>
        </div>
        <div class="flex px-5 py-6 border-b">
            <div class="w-1/6 my-auto">
                <label class="text-gray-600">Автомобиль</label>
            </div>
            <select class="form-input border p-3 text-sm w-5/6" wire:model.lazy='selectedCarIndex'
                wire:click="carChange">
                @foreach ($carsData as $key => $car)
                    <option value="{{ $key }}">
                        {{ $car['model'] . ' / ' . $car['year'] . ' / ' . $car['color'] }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="flex px-5 py-6 border-b">
            <div class="w-1/6 my-auto">
                <label class="text-gray-600">Дата покупки</label>
            </div>
            <input class="form-input p-3 w-2/8" type="date" wire:model="buyDate" name="dt" />
        </div>
        <div class="flex px-5 py-6 border-b">
            <div class="w-1/6 my-auto">
                <label class="text-gray-600">Цена</label>
            </div>
            <input class="form-input p-3 w-1/8" wire:model.defer="price" />
            <div class="my-auto px-2">руб.</div>
        </div>
        <div class="flex px-5 py-6 border-b">
            <div class="w-1/6 my-auto">
                <label class="text-gray-600">Описание</label>
            </div>
            <textarea class="form-input p-3 w-5/6" rows="2" wire:model.defer="description"></textarea>
        </div>
        <div class="flex px-5 py-6 border-b">
            <div class="w-1/6 my-auto">
                <label class="text-gray-600">Файлы</label>
            </div>
            <div class="w-5/6 font-bold">
                <x-file-storage-view :storageType="\App\Models\File::FILE_TYPE_RESALE_PROJECT"
                    :storageFilesStatus="\App\Models\File::FILE_STATUS_WAITING" addButton="Добавить">
                </x-file-storage-view>
            </div>
        </div>
        <div class="text-center p-5">
            <x-button type="submit">Создать</x-button>
        </div>

    </form>
</x-forms.form-frame>
