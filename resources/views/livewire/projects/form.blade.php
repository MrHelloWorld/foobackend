<div>
    <div class="text-center mb-4">
        <a href="{{ route('projects.add') }}">
            <x-button>Новый проект</x-button>
        </a>
    </div>
    <div class="flex flex-col" x-data="{
         showDialog: false,
         projectId: -1
         }">
        <div class="shadow overflow-hidden border-gray-200">
            <table class="border">
                <thead class="bg-gray-50 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">
                    <tr>
                        <th scope="col" class="px-6 py-3 w-2/6">Инвестор</th>
                        <th scope="col" class="w-2/6">Автомобиль</th>
                        <th scope="col" class="text-center w-1/6">Статус</th>
                        <th class="col" scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($this->resaleProjects as $key => $project)
                        @php
                            if ($project['seller_id'] != null) {
                                $highlighted = 'bg-green-100';
                            } else {
                                $highlighted = $key % 2 ? 'bg-gray-50' : '';
                            }
                        @endphp
                        <tr class="border {{ $highlighted }}">
                            <td class="px-6 py-4">
                                <div class="font-semibold">
                                    {{ $project['investor']['name'] . ' ' . $project['investor']['surname'] }}</div>
                                <div class="text-gray-500 text-sm mb-2">{{ $project['investor']['phone'] }}</div>
                                <div class="font-semibold text-blue-900">Счёт: {{ $project['investor']['balance'] }}
                                    р.</div>
                            </td>
                            <td>
                                <div>{{ $project['car']['model'] }}</div>
                                <div class="text-gray-500 text-sm mb-2">{{ $project['car']['color'] }},
                                    {{ $project['car']['year'] }} г.</div>
                                <div class="text-gray-800 font-semibold">Куплен: {{ $project['car']['buy_price'] }}
                                    руб.</div>
                            </td>
                            <td class="text-center font-semibold">
                                @if ($project['seller_id'] != null)
                                    <div>Продан</div>
                                    <div class="text-xl">{{ $project['sell_price'] }} руб.</div>
                                @else
                                    <div class="font-semibold">Продажа...</div>
                                @endif
                            </td>
                            <td class="text-right">
                                <div class="text-center p-2">
                                    <a href="{{ route('projects.details', ['id' => $project['id']]) }}">
                                        <x-button class="text-sm m-1">Подробнее...</x-button>
                                    </a>
                                    <x-button wire:click="deleteProject({{ $project['id'] }})" class="text-sm">
                                        Удалить</x-button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    <livewire:components.modal-dialog :name="$deleteModalName" :header="'Вопрос'"
                        :body="'Удалить текущий проект и все работы, связанные с ним?'"
                        :type="App\Http\Livewire\Components\ModalDialog::MODAL_DIALOG_YES_NO" />
                </tbody>
            </table>
        </div>
    </div>
</div>
