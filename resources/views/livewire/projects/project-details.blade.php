<div class=""
     x-data="{showTab: 'project'}">
    <div class="my-auto pt-8 border-b px-10 flex justify-between pb-2">
        <div class="font-semibold tracking-wider text-lg">
            {{$carProperties['model'].', '.$carProperties['year']}} г.
            <span class="px-8 font-semibold tracking-wider text-2xl">
                @if ($leftId != -1)
                <a href="{{ route('projects.details',['id'=>$leftId]) }}" class="hover:bg-gray-200">
                    <
                </a>
                @endif
                @if ($rightId != -1)

                <a href="{{ route('projects.details',['id'=>$rightId]) }}" class="hover:bg-gray-200">
                    >
                </a>
                @endif
            </span>
        </div>
        <div class="text-right">
            @if (!$carProperties['isSold'])
            <x-button wire:click="showAddOrderForm">Доб. работы</x-button>
            <x-button wire:click="showSellCarForm">Продать</x-button>
            @else
            @endif
            <x-button wire:click="showDeleteDialog">Удалить</x-button>
        </div>
    </div>
    <div class="mx-10 my-4">
        <ul class="flex pt-3  border-b tracking-tight">
            <li class="px-4 py-2 text-gray-600 cursor-pointer hover:bg-gray-200 border-"
                :class="{'border-gray-600 border-b-2': showTab == 'params'}"
                @click="showTab = 'params'">
                Характеристики
            </li>
            <li class="px-4 py-2 text-gray-600 cursor-pointer hover:bg-gray-200 border-"
                :class="{'border-gray-600 border-b-2': showTab == 'project'}"
                @click="showTab = 'project'">
                Проект
            </li>
            <li class="px-4 py-2 text-gray-600 cursor-pointer hover:bg-gray-200 border-"
                :class="{'border-gray-600 border-b-2': showTab == 'orders'}"
                @click="showTab = 'orders'">
                Работы
            </li>
        </ul>
        <div class="m-5">
            <div x-show="showTab == 'params'">
                <livewire:cars.partial.car-details :carId="$carId"
                                                          :car="$carProperties" />
            </div>
            <div x-show="showTab == 'project'">
                <livewire:projects.partial.project-details :projectId="$projectId"
                                                           :car="$carProperties" />
            </div>
            <div x-show="showTab == 'orders'">
                <livewire:working-orders.orders-table :carId="$carId" />
            </div>
        </div>
    </div>
    <livewire:working-orders.add-form :resaleProjectId="$projectId"/>
    <livewire:working-orders.sell-form :resaleProjectId="$projectId"/>
    <livewire:components.modal-dialog :name="'deleteProject'"
                                      :type="App\Http\Livewire\Components\ModalDialog::MODAL_DIALOG_YES_NO"
                                      :header="'Вопрос'"
                                      :body="'Удалить текущий проект и все работы, связанные с ним?'"/>
</div>