<div>
    <div x-data="{editMode: false}">
        <div class="flex" x-show="!editMode">
            <div id="{{ '_' . $uid }}" class="pr-4">{{ $value }}</div>
            <button class="border rounded-lg shadow-md bg-blue-100 px-2" @click="
                    editMode = true;
                    editButtonClick('{{ $uid }}');
                ">...</button>
        </div>
        <div class="flex my-auto" x-show="editMode">
            <div class="my-auto ">
                <input id="{{ $uid }}" name="{{ $uid }}"
                    class="form-input mx-1 border-b h-7 flex-grow w-full" value="" wire:model.lazy="value" />
                @if ($errors->has('value'))
                    @php $this->value = $this->initValue; @endphp
                    {{ 'Неверное значение' }}
                    <script>
                        resetInput('{{ $uid }}', '{{ $this->initValue }}')

                    </script>
                @endif

            </div>
            <button class="ml-3 border rounded-lg shadow-md bg-green-400 whitespace-pre"
                @click="editMode=false; acceptButtonClick('{{ $uid }}');" wire:click='acceptClick'> ✔ </button>
            <button class="border rounded-lg shadow-md bg-red-400 whitespace-pre" @click="editMode=false"> ✖ </button>
        </div>
    </div>
</div>
