<div>
    <div class="text-center mb-4">
        <a href="{{ route('cars.add') }}">
            <x-button>Добавить</x-button>
        </a>
    </div>
    <div class="flex flex-col">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">
                <tr class="border">
                    <th scope="col" class="px-6 py-3 w-1/6">Автомобиль</th>
                    <th scope="col" class="w-1/6">Добавил</th>
                    <th scope="col" class="w-1/6">Купил</th>
                    <th scope="col" class="text-center w-1/6">Продал</th>
                    <th scope="col" class="w-1/12"></th>
                </tr>
            </thead>
            <livewire:cars.cars-details/>
            <tbody class="bg-white divide-y divide-gray-200 px-6 py-4 text-s whitespace-nowrap">
                @foreach ($this->carsProperties as $key => $car)
                    @php
                        if ($car['isSold']) {
                            $highlighted = 'bg-green-200';
                        } else {
                            $highlighted = $key % 2 ? 'bg-gray-50' : '';
                        }
                    @endphp

                    <tr class="border {{ $highlighted }}">
                        <td class="px-6 py-4 relative">
                            <div class="uppercase font-semibold">{{ $car['model'] }}</div>
                            <div class="text-gray-500 text-sm mb-2">{{ $car['color'] }}, {{ $car['year'] }} г.</div>
                            <div class="bg-gray-800 cursor-pointer text-white text-center rounded-md m- px-2"
                                wire:click="deleteCar({{ $car['id'] }})">
                                Удалить
                            </div>
                        </td>
                        <td>
                            <div class="font-semibold">
                                {{ $car['creator_name'] . ' ' . $car['creator_surname'] }}
                            </div>
                            <div class="text-gray-500 text-sm mb-2">
                                {{ App\Helpers\HelpTo::getHumanDateTime($car['created_at']) }}</div>
                            <div class="text-gray-800 font-semibold"></div>
                        </td>
                        <td>
                            <div class="font-semibold">
                                {{ $car['buyer_name'] . ' ' }}{{ $car['buyer_surname'] }}
                            </div>
                            <div class="text-gray-500 text-sm mb-2">
                                {{ App\Helpers\HelpTo::getHumanDate($car['buy_datetime']) }}</div>
                            <div class="text-gray-800 font-semibold"></div>
                        </td>
                        <td class="text-center">
                            <div class="font-semibold">
                                {{ $car['seller_name'] . ' ' }}{{ $car['seller_surname'] }}
                            </div>
                            <div class="text-gray-500 text-sm mb-2">
                                {{ App\Helpers\HelpTo::getHumanDate($car['sell_datetime']) }}</div>
                        </td>
                        <td class="text-right relative">
                            <div class="text-center p-2">
                                <x-button wire:click="showCarDetails({{ $car['id'] }})">
                                    Подробнее...
                                </x-button>
                            </div>
                            <div class="text-center p-2">
                                @if ($car['buy_datetime'] == '')
                                    <x-button wire:click="addProject">Проект...</x-button>
                                @else
                                    <a href="{{ route('projects.details', ['id' => $car['project_id']]) }}">
                                        <x-button>Проект...</x-button>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <livewire:components.modal-dialog :name="$deleteCarModalName" :header="'Вопрос'"
        :body="'Удалить машину? (будут удалены проект и все ордера по работам со всеми данными)'"
        :type="App\Http\Livewire\Components\ModalDialog::MODAL_DIALOG_YES_NO" />

    <livewire:components.modal-dialog :name="$createProjectModalName" :header="'Вопрос'"
        :body="'Проект еще не создан. Создать?'"
        :type="App\Http\Livewire\Components\ModalDialog::MODAL_DIALOG_YES_NO" />

</div>
