<div class="text-left">
    <!-- ROW -->
    <div wire:loading class="text-center text-green-500 w-full">Загрузка...</div>
    <div class="flex px-5 pt-5 border-b">
        <div class="w-1/6 my-4 text-gray-500">Модель</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['model']"
                                   :emitEvent="'modelWasChanged'"
                                   :validateRule="$rules['model']"
                                   key="{{ time().$carId }}">
        </div>
        <div class="w-1/6 my-4 text-gray-500">Год выпуска</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['year']"
                                   :emitEvent="'yearWasChanged'"
                                   :validateRule="$rules['year']"
                                   key="{{ time().$carId }}">

        </div>
    </div>
    <!-- ROW -->
    <div class="flex px-5 border-b">
        <div class="w-1/6 my-4 text-gray-500">Тип/Категория</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['type']"
                                   :emitEvent="'typeWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
        <div class="w-1/6 my-4 text-gray-500">Цвет</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['color']"
                                   :emitEvent="'colorWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
    </div>
    <!-- ROW -->
    <div class="flex px-5 border-b">
        <div class="w-1/6 my-4 text-gray-500">Регистрационный знак</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['reg_number']"
                                   :emitEvent="'reg_numberWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
        <div class="w-1/6 my-4 text-gray-500">Номер VIN</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['vin']"
                                   :emitEvent="'vinWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
    </div>
    <!-- ROW -->
    <div class="flex px-5 border-b">
        <div class="w-1/6 my-4 text-gray-500">Кузов</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['kuzov_number']"
                                   :emitEvent="'kuzov_numberWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
        <div class="w-1/6 my-4 text-gray-500">Шасси/Рама</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['rama_number']"
                                   :emitEvent="'rama_numberWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
    </div>
    <!-- ROW -->
    <div class="flex px-5 border-b">
        <div class="w-1/6 my-4 text-gray-500">Мощность, лс</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['power_horses']"
                                   :emitEvent="'power_horsesWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
        <div class="w-1/6 my-4 text-gray-500">Мощность, квт</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['power_kvt']"
                                   :emitEvent="'power_kvtWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
    </div>
    <!-- ROW -->
    <div class="flex px-5 border-b">
        <div class="w-1/6 my-4 text-gray-500">Масса</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['mass']"
                                   :emitEvent="'massWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
        <div class="w-1/6 my-4 text-gray-500">Цена при добавлении</div>
        <div class="w-2/6 text-gray-800 my-auto">
            <livewire:edit-element :value="$car['buy_price']"
                                   :emitEvent="'buy_priceWasChanged'"
                                   key="{{ time().$carId }}">
        </div>
    </div>
</div>
