<div x-data="{}">
    <div x-show="$wire.visible">
        <div class="fixed bg-gray-600 top-0 left-0 bottom-0 right-0 bg-opacity-50"></div>
        <div class="bg-white fixed left-2/12 top-1/2 z-50 w-4/6 rounded-xl"
             style="transform: translate(-0%, -50%);">
             <span wire:click="hide" class="float-right text-2xl cursor-pointer p-4">&times </span>
             <x-forms.form-caption mainLabel="Данные автомобиля" />
            <livewire:cars.partial.car-details :carId="$carId" key="{{ Str::random() }}"/>
            <div class="text-center p-5">
                <x-button wire:click="hide">OK</x-button>
            </div>
        </div>
    </div>
</div>
