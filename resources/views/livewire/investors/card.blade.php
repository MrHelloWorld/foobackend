<div class="m-auto shadow-lg rounded-lg border bg-gray-100" x-data="{}">
    <div class="flex flex-col pt-2 px-4">
        <div class="flex">
            <div class="font-semibold tracking-wider px-2">
                {{ $investorProperties['name'] . ' ' . $investorProperties['surname'] }}</div>
        </div>
        <div class="flex">
            <div class="text-s text-gray-600 tracking-tight px-2 pt-1 w-1/3"> {{ $investorProperties['phone'] }}</div>
            <div class="text-s text-gray-600 tracking-tight px-2 pt-1 w-1/3">{{ 'Добавлен:' }}</div>
            <div class="text-xl font-bold text-blue-900">Счёт: {{ $service->calcBalance($investorId) }} руб.</div>
        </div>

        <div class="flex">
            <div class="text-sm text-gray-600 tracking-tight px-2 pb-3 w-1/3">{{ $investorProperties['email'] }}</div>
            <div class="text-sm text-gray-600 tracking-tight px-2 pb-3 w-1/3">
                {{ $userProperties['name'] . ' ' . $userProperties['surname'] }}</div>
        </div>
        <div class="flex text-center text-sm cursor-pointer">
            <div wire:click="select({{ 1 }})" @click="if($wire.selected == 1) {$wire.selected = -1;}" class="w-1/3 bg-gray-200 hover:bg-gray-700 border hover:text-white rounded-t-md py-2
                 @if ($selected==1) {{ 'bg-gray-700 text-white' }} @endif">
                Cчет
            </div>
            <div wire:click="select({{ 2 }})" @click="if($wire.selected == 2) {$wire.selected = -1;}" class="w-1/3 bg-gray-200 hover:bg-gray-700 border hover:text-white rounded-t-md py-2
                 @if ($selected==2) {{ 'bg-gray-700 text-white' }} @endif">
                Проекты
            </div>
            <!--            <div wire:click="select({{ 3 }})"
                 class="w-1/3 bg-gray-200 hover:bg-gray-700 border hover:text-white rounded-t-md py-2
                 /*@if ($selected == 3) {{ 'bg-gray-700 text-white' }} @endif"*/
                 >
            </div>-->
        </div>
        <div x-show="$wire.selected == 1">
            <div class="my-5">
                <table class="min-w-full divide-y divide-gray-200 my-4 bg-white ">
                    <thead class="bg-gray-100 font-medium text-xl tracking-wider">
                        <tr class="">
                            <th class="pt-4 text-left w-1/3" scope="col">Операции по счету</th>
                            <th class="w-1/3"></th>
                            <th class="w-1/3"></th>
                        </tr>
                        <tr
                            class="border uppercase bg-gray-50 text-center text-sm text-gray-700 font-bold tracking-wider">
                            <th scope="col" class="w-3/6">Операция</th>
                            <th scope="col" class="w-1/6">Дата</th>
                            <th scope="col" class="w-1/6 p-2">Сумма</th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200 px-6 py-4 text-s whitespace-nowrap">
                        @foreach ($this->accountOperations as $key => $operation)
                            @php
                                if ($operation->amount >= 0) {
                                    $highlighted = 'bg-green-100';
                                } else {
                                    $highlighted = 'bg-red-100';
                                }
                            @endphp
                            <tr class="border text-sm {{ $highlighted }}">
                                <td class="p-2">
                                    <div class="">{{ $operation->operation_name }}</div>
                                </td>
                                <td class="text-center">
                                    <div class="">{{ $operation->timestamp }}</div>
                                </td>
                                <td class="text-center font-semibold ">
                                    <div class="font-semibold">@php echo $operation->amount > 0 ? "+$operation->amount" : "$operation->amount" @endphp</div>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>
                                <div class="my-auto text-center p-4">
                                    <x-button wire:click="showDepositDialog">Внести</x-button>
                                    <x-button wire:click="showWithdrawDialog">Вывести</x-button>
                                </div>
                            </td>
                        </tr>
                        <!------------Projects--------------->
                        <thead class="bg-gray-100 font-medium text-xl tracking-wider">
                            <tr class="">
                                <th class="pt-5 text-left w-1/3" scope="col">Операции по проектам</th>
                                <th class="w-1/3"></th>
                                <th class="w-1/3"></th>
                            </tr>
                            <tr
                                class="border uppercase bg-gray-50 text-center text-sm text-gray-700 font-bold tracking-wider">
                                <th scope="col" class="w-3/6">Операция</th>
                                <th scope="col" class="w-1/6">Дата</th>
                                <th scope="col" class="w-1/6 p-2">Сумма</th>
                            </tr>
                        </thead>
                        @foreach ($this->resaleProjects as $key => $project)
                            <tr class="bg-white border">
                                <div>
                                    <td class="font-semibold text-center pt-5">
                                        {{ $project->car->model }}. {{ $project->car->color }},
                                        {{ $project->car->year }} г.
                                    </td>
                                </div>
                            </tr>
                            <tr class="border bg-red-100 text-sm">
                                <td class="p-1">
                                    Покупка
                                </td>
                                <td class="text-center">
                                    {{ App\Helpers\HelpTo::getHumanDate($project->buy_datetime) }}
                                </td>
                                <td class="text-center font-semibold">
                                    -{{ $project->car->buy_price }} руб.
                                </td>
                            </tr>
                            @foreach ($project->working_order as $order)
                                @if ($order->type == App\Models\WorkingOrder::ORDER_TYPE_SALE)
                                    <tr class="border bg-green-100 text-sm">
                                    @else
                                    <tr class="border bg-red-100 text-sm">
                                @endif
                                <!--<tr class="border">-->
                                <td class="p-1">
                                    {{ $order->description }}
                                    @if ($order->end_datetime == null && $order->type != App\Models\WorkingOrder::ORDER_TYPE_SALE)
                                        (в работе)
                                    @endif
                                </td>
                                <td class="text-center">

                                    {{ App\Helpers\HelpTo::getHumanDateTime($order->created_at) }}
                                </td>
                                <td class="text-center">
                                    @if ($order->type == App\Models\WorkingOrder::ORDER_TYPE_SALE)
                                        <span class="bg-green-100 font-semibold">+{{ $order->price }} руб.</span>
                                    @else
                                        <span class="bg-red-100 font-semibold">-{{ $order->price }} руб.</span>
                                    @endif
                                    @if ($order->end_datetime == null && $order->type != App\Models\WorkingOrder::ORDER_TYPE_SALE)
                                        (предварительно)
                                    @endif
                                </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div x-show='$wire.selected == 2' class="mb-3 border-b">
            <div class="">
                <table class="min-w-full divide-y divide-gray-200">
                    <!--                    <thead class="bg-gray-50 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">
                        <tr class="border-r border-l">
                            <th scope="col" class="w-2/6 p-5">Автомобиль</th>
                            <th scope="col" class="text-center w-1/6">Статус</th>
                            <th scope="col" class="text-center w-1/6" >Детали</th>
                        </tr>
                    </thead>-->
                    <tbody class="bg-white divide-y divide-gray-200 px-6 py-4 text-s whitespace-nowrap">
                        @foreach ($this->resaleProjects as $key => $project)
                            @php
                                if ($project->seller_id != null) {
                                    $highlighted = 'bg-green-100';
                                } else {
                                    $highlighted = $key % 2 ? 'bg-gray-100' : '';
                                }
                            @endphp
                            <tr class="border-l border-r border-b {{ $highlighted }}">
                                <td class="p-2">
                                    <div class="uppercase font-semibold">{{ $project->car->model }}</div>
                                    <div class="text-gray-500 text-sm mb-2">{{ $project->car->color }},
                                        {{ $project->car->year }} г.</div>
                                    <div class="text-gray-800 font-semibold">Куплен: {{ $project->car->buy_price }}
                                        руб.</div>
                                </td>
                                <td class="text-center">
                                    @if ($project->seller_id != null)
                                        <div class="uppercase font-semibold">Продан</div>
                                        <div class="uppercase font-semibold text-xl">{{ $project->sell_price }} руб.
                                        </div>
                                    @else
                                        <span class="underline">Статус:</span>
                                        <div class="font-semibold">Продажа...</div>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <div class="text-center p-2">
                                        <a href="{{ route('projects.details', ['id' => $project->id]) }}">
                                            <x-button class="text-sm m-1">Подробнее...</x-button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div x-show='$wire.selected == 3'>

        </div>
    </div>
    <livewire:components.input-dialog :name="$depositDialogName" :header="'Внесение средств'" :inputCaption="'Cумма:'"
        :inputText="0" :validationRule="$depositDialogValidatioRule" />
    <livewire:components.input-dialog :name="$withdrawDialogName" :header="'Вывод средств'" :inputCaption="'Cумма:'"
        :inputText="0" :validationRule="$depositDialogValidatioRule" />
</div>
