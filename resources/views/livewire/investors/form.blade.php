<div>
    <div class="text-center mb-4">
        <x-button wire:click="showAddForm">Добавить</x-button>
    </div>
    <livewire:investors.add-form />
    @foreach ($this->investorsData as $inv)
        <div class="m-1 mb-3">
            @livewire('investors.card', ['investorId' => $inv['id']], key($loop->index))
        </div>
    @endforeach
</div>
