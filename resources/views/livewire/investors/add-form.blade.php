<div>
    <x-jet-modal max-width="xl" wire:model="showForm">
        <x-forms.form-frame>
            <x-forms.form-caption mainLabel="Введите данные" secondLabel="для добавления инвестора"/>
            <form wire:submit.prevent="submit">
                <x-forms.wire-input-error labelText="Имя" wireModel="name"/>
                <x-forms.wire-input-error labelText="Фамилия" wireModel="surname"/>
                <x-forms.wire-input-error labelText="Email" wireModel="email"/>
                <x-forms.wire-input-error labelText="Телефон" wireModel="phone"/>
                <x-forms.wire-input-error labelText="Баланс" wireModel="balance"/>
                <x-forms.wire-input-error labelText="Логин" wireModel="login"/>
                <div class="flex">
                    <x-forms.wire-input-error labelText="Пароль" wireModel="password" inputType="password"/>
                    <x-forms.wire-input-error labelText="Еще раз" wireModel="password_confirmation" inputType="password"/>
                </div>
                <div class="text-center p-5">
                    <x-button type="submit">Добавить</x-button>
                </div>
            </form>
        </x-forms.form-frame>
    </x-jet-modal>
</div>
