<div>
    <x-jet-modal wire:model="visible" >
        <div class="">
            <div class="bg-white border shadow-lg rounded-xl">
                <div class="text-center font-bold text-xl bg p-3 border-b">
                    {{ $header }}
                    <span wire:click="hide"  class="float-right text-xl cursor-pointer">&times </span>
                </div>
                <div class="text-center text-xl p-5">
                    <span class="pr-3">{{ $inputCaption }}</span>
                    <input class="form-input"
                           type="type" 
                           name="name" 
                           wire:model='inputText'>
                    @error('0')
                    <div class="text-sm text-red-600">Неверное значение</div>
                    @endif
                </div>
                <div class="text-center p-5 text-sm">
                    <x-button wire:click='clickedOk'>ОК</x-button>
                    <x-button wire:click='clickedCancel'>Отмена</x-button>
                </div>
            </div>
        </div>
    </x-jet-modal>
</div>