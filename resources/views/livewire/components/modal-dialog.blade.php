<div>
    <x-jet-modal wire:model="visible" >
        <div class="">
            <div class="bg-white border shadow-lg rounded-xl">
                <div class="text-center font-bold text-xl bg p-3 border-b">
                    {{ $header }}
                    <span wire:click="hide"  class="float-right text-xl cursor-pointer">&times </span>
                </div>
                <div class="text-center text-xl p-3">
                    <span>{{ $body }}</span>
                </div>
                <div class="text-center p-5 text-sm">
                    @if($type == App\Http\Livewire\Components\ModalDialog::MODAL_DIALOG_YES_NO)
                        <x-button wire:click='clickedYes'>Да</x-button>
                        <x-button wire:click='clickedNo'>Нет</x-button>
                    @else
                        <x-button wire:click='clickedOk'>OK</x-button>
                    @endif
                </div>
            </div>
        </div>
    </x-jet-modal>
</div>