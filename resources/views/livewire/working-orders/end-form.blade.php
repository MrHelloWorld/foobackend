<x-jet-modal wire:model="visible">
    <x-forms.form-frame>
        <x-forms.form-caption mainLabel="Введите данные" secondLabel="для завершения работ" />
        <form wire:submit.prevent="submit">
            <div class="flex px-5 py-6 border-b">
                <div class="w-1/6 my-auto">
                    <label class="text-gray-600">Дата окончания</label>
                </div>
                <input class="form-input p-3 w-2/8" type="date" wire:model="endDatetime" name="dt" />
            </div>
            <div class="flex px-5 py-6 border-b">
                <div class="w-1/6 my-auto">
                    <label class="text-gray-600">Цена</label>
                    @error('price')<div class="text-red-500 text-sm">Неверное значение</div> @enderror
                </div>
                <input class="form-input p-3 w-2/12" wire:model.defer="price" />
                <div class="my-auto px-2">руб.</div>
            </div>
            <div class="flex px-5 py-6 border-b">
                <div class="w-1/6 my-auto">
                    <label class="text-gray-600">Файлы</label>
                </div>
                <div class="w-5/6">
                    <x-file-storage-view :storageType="App\Helpers\FileStorage::STORAGE_TYPE_WORKING_ORDER"
                        :storageFilesStatus="App\Models\File::FILE_STATUS_UPLOADED" :id="$orderId" addButton="Добавить">
                    </x-file-storage-view>
                </div>
            </div>
            <div class="text-center p-5">
                <x-button type="submit">Завершить</x-button>
            </div>
        </form>
    </x-forms.form-frame>
</x-jet-modal>
