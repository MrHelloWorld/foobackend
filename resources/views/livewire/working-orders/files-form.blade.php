<x-jet-modal wire:model="visible">
    <div class="float-right cursor-pointer px-1 m-2"
         wire:click="hide">
        &#10006
    </div><br>
    <div class="p-4">
        <x-file-storage-view :storageType="App\Helpers\FileStorage::STORAGE_TYPE_WORKING_ORDER"
                             :storageFilesStatus="App\Models\File::FILE_STATUS_UPLOADED"
                             :id="$orderId"
                             addButton="Добавить">        
        </x-file-storage-view>
    </div>
</x-jet-modal>