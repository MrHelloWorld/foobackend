<div>
    <table class="min-w-ful divide- divide-gray-200 table-fixed">
        <thead class="bg-gray-50 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">
            <tr class="border">
                <th scope="col" class="px-6 py-3 w-1/6">Добавил</th>
                <th scope="col" class="w-2/6">Описание</th>
                <th scope="col" class="text-center w-1/6">Сроки</th>
                <th scope="col" class="text-center w-1/6">Стоимость</th>
                <th scope="col" class="text-center"></th>
            </tr>
        </thead>
        <tbody class="bg-white divide- divide-gray-200 px-6 py-4 text-s whitespace-nowrap">
            @foreach($orders as $order)
            <tr class="border">
                <td class="px-6 py-4">
                    <div class="">
                        {{ $order['createdUserProps']['name'].' '.$order['createdUserProps']['surname']}}
                    </div>
                    <!--<div class="text-gray-500 text-sm mb-2">12.12.2042 г.</div>-->
                </td>
                <td>
                    @if($order['type'] == App\Models\WorkingOrder::ORDER_TYPE_WORK)
                    <div class="font-semibold">Работы</div>
                    @else
                    <div class="font-semibold">Продажа</div>
                    @endif
                    <div class="whitespace-normal">{{ $order['description'] }}</div>
                </td>
                <td class="text-center">
                    <div class="font-semibold">{{ App\Helpers\HelpTo::getHumanDate($order['start_datetime']) }}</div>
                    @if($order['type'] == App\Models\WorkingOrder::ORDER_TYPE_WORK)
                        @if($order['end_datetime'] != null )
                            <div class="font-semibold">{{ App\Helpers\HelpTo::getHumanDate($order['end_datetime']) }}</div>
                        @else  
                            <x-button wire:click="showEndOrderForm({{ $order['id'] }})" class="text-sm">Завершить</x-button>
                        @endif
                    @endif
        </td>
        <td class="text-center">
            <div class="font-semibold">{{ $order['price'] }} руб.</div>
            @if($order['type'] == App\Models\WorkingOrder::ORDER_TYPE_WORK && 
                $order['end_datetime'] != null)
                <div class="text-sm">(дней работ: {{ App\Helpers\HelpTo::daysBetweenDatabaseDates($order['start_datetime'], $order['end_datetime'])+1 }})</div>
            @endif            
        </td>
        <td class="text-right">
            <div class="text-right text-sm m-3">
                <x-button wire:click="showFilesForm({{$order['id']}})" class="my-1">Файлы...</x-button>
                <br>
                <x-button wire:click="deleteOrder({{$order['id']}})" class="">Удалить</x-button>
            </div>
        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <livewire:working-orders.end-form/>
    <livewire:working-orders.files-form/>
    <livewire:components.modal-dialog :name="$deleteOrderModalName"
                                      :header="'Вопрос'"
                                      :body="'Удалить текущую позицию?'"
                                      :type="App\Http\Livewire\Components\ModalDialog::MODAL_DIALOG_YES_NO"/>
</div>