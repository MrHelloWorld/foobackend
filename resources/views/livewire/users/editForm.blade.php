<div>
    <x-jet-modal max-width="xl" wire:model="showForm">
            <x-forms.form-caption mainLabel="Изменение профиля" secondLabel="Введите новые данные"/>
            <form wire:submit.prevent="submit">
                <x-forms.wire-input-error labelWidth="w-2/6" inputWidth="w-4/6" labelText="Имя" wireModel="userProperties.name"/>
                <x-forms.wire-input-error labelWidth="w-2/6" inputWidth="w-4/6" labelText="Фамилия" wireModel="userProperties.surname"/>
                <x-forms.wire-input-error labelWidth="w-2/6" inputWidth="w-4/6" labelText="Должность" wireModel="userProperties.position"/>

                <div class="flex px-5 py-3 border-b">
                    <div class="w-2/6 my-auto">
                        <label class="text-gray-600">Права</label>
                    </div>
                    <select class="form-input border p-2 w-4/6" wire:model.defer="userProperties.role">
                        <option value="1">Администратор</option>
                        <option value="2">Выкупщик</option>
                        <option value="3">Продавец</option>
                    </select>
                </div>
                @error('role')<div class="text-red-500 text-right text-xs -mt-2">{{ $message }}</div>@enderror

                <x-forms.wire-input-error labelWidth="w-2/6" inputWidth="w-4/6" labelText="Почта" wireModel="userProperties.email"/>
                <div class="flex">
                    <x-forms.wire-input-error labelWidth="w-2/6" inputWidth="w-4/6" labelText="Пароль" wireModel="password" inputType="password"/>
                    <x-forms.wire-input-error labelWidth="w-2/6" inputWidth="w-4/6" labelText="Еще раз" wireModel="password_confirmation" inputType="password"/>
                </div>
                <div class="text-center p-5">
                    <x-button type="submit">Изменить</x-button>
                </div>
            </form>
    </x-jet-modal>
</div>
