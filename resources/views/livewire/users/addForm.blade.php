<div>
    <x-jet-modal max-width="xlcc" wire:model="showForm">
        <x-forms.form-frame>
            <x-forms.form-caption mainLabel="Введите данные" secondLabel="для добавления пользователя" />
            <form wire:submit.prevent="submit">
                <x-forms.wire-input-error labelText="Имя" wireModel="name" />
                <x-forms.wire-input-error labelText="Фамилия" wireModel="surname" />
                <x-forms.wire-input-error labelText="Должность" wireModel="position" />
                <div class="flex px-5 py-3 border-b">
                    <div class="w-2/6 my-auto">
                        <label class="text-gray-600">Права</label>
                    </div>
                    <select class="form-input border p-2 w-4/6" wire:model.defer="role">
                        <option value="1">Администратор</option>
                        <option value="2">Выкупщик</option>
                        <option value="3">Продавец</option>
                    </select>
                </div>
                @error('role')<div class="text-red-500 text-right text-xs -mt-2">{{ $message }}</div>@enderror

                <x-forms.wire-input-error labelText="Почта" wireModel="email" />
                <div class="flex">
                    <x-forms.wire-input-error labelText="Пароль" wireModel="password" inputType="password" />
                    <x-forms.wire-input-error labelText="Еще раз" wireModel="password_confirmation"
                        inputType="password" />
                </div>
                <div class="text-center p-5">
                    <x-button type="submit">Добавить</x-button>
                </div>
            </form>
        </x-forms.form-frame>
    </x-jet-modal>
</div>
