<div>
    <div class="text-center mb-4">
        <x-button wire:click="showAddForm">Добавить</x-button>
    </div>
    <livewire:users.add-form />
    <livewire:users.edit-form />
    <div class="flex flex-col">
            <table class="border">
                <thead class="bg-gray-50 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">
                    <tr>
                        <th scope="col" class="px-6 py-3 w-3/6">Имя</th>
                        <th scope="col" class="w-2/6">Должность</th>
                        <th scope="col" class="w-1/12">Профиль</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usersProperties as $user)
                    <tr class="border" >
                        <td class="px-6 py-4">
                            <div class="font-medium"> {{ $user['name'].' '.$user['surname'] }}</div>
                            <div class="text-gray-500">{{ $user['email'] }}</div>
                        </td>
                        <td>
                            <div>{{ $user['position'] }}</div>
                            <div class="text-gray-500">Права: {{ App\Models\User::USER_ROLES[$user['role']] }}</div>
                        </td>
                        <td class="text-gray-500">
                            <x-button wire:click="showEditForm({{ $user['id'] }})">Изменить</x-button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
