@props([
    'labelWidth' => 'w-2/6',
    'inputWidth' => 'w-4/6',
    'labelText' => '',
    'name' => '',
    'width' => ''
])
@php
 $value = old('$name');
 @endphp

 <div class="border-b {{ $width }}">
     <div class="flex px-5 py-3">
         <div class="{{ $labelWidth }} my-auto text-gray-600">
             <label>{{ $labelText }} </label>
             @php
             if ($errors->has($name)) {
                echo "<div class='text-red-500 text-left text-xs'>".$errors->first($name)."</div>";
             }
             @endphp
         </div>
         <input class="form-input p-2 {{ $inputWidth }}" name="{{ $name }}" value="$value"/>
     </div>
 </div>
