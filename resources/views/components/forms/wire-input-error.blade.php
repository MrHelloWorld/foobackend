@props([
    'labelWidth' => 'w-2/6',
    'inputWidth' => 'w-4/6',
    'labelText' => '',
    'wireModel' => '',
    'inputType' => ''
])
<div class="border-b">
    <div class="flex px-5 py-3">
        <div class="{{ $labelWidth }} my-auto">
            <label class="text-gray-600">{{ $labelText }}</label>
        </div>
        <input class="form-input p-2 {{ $inputWidth }}" wire:model.defer="{{ $wireModel }}" type="{{ $inputType }}"/>
    </div>
    @php
    if ($errors->has($wireModel)) {
        echo "<div class='text-red-500 text-center text-xs -mt-2 mb-2'>".$errors->first($wireModel)."</div>";
    }
    @endphp
</div>