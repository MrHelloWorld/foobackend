@props([
    'maxWidth' => 'max-w-5xl',
])
<div class="{{ $maxWidth }} mx-auto">
    <div class="tracking-tight bg-white shadow-md rounded-md border">
        {{ $slot }}
    </div>
</div>
