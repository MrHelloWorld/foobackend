@props([
    'mainLabel' => '',
    'secondLabel' => '',
])
<div class="px-5 border-b pt-5 rounded-t-xl tracking-tight bg-gray-50">
    <p class="tracking-wide text-xl">{{ $mainLabel }}</p>
    <p class="text-sm text-gray-500 pb-6">{{ $secondLabel }}</p>
</div>