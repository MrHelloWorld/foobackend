<button {{ $attributes->merge([
        'type' => 'submit', 
        'class' => 'px-6 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-white hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150'
        ])}}
    >
    {{ $slot }}
</button>
