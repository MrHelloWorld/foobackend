@props(['href' => '#'])
<a href="{{$href}}">
    <div {{ 
         $attributes->merge(['class'=>'flex-none px-4 py-2 rounded-sm hover:text-white hover:bg-gray-700'])
        }}>

        {{ $slot }}

    </div>
</a>