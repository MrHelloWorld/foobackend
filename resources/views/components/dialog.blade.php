<div class="">
    <div class="fixed bg-gray-600 top-0 left-0 bottom-0 right-0 bg-opacity-50"></div>
    <div class="bg-white fixed left-2/12 top-1/2 z-50 w-4/6" style="transform: translate(-0%, -50%);">
        <div class="tracking-tight bg-white shadow-xl rounded-xl">
            <div class="px-5 border-b pt-5 tracking-tight bg-gray-50">
                <span class="float-right text-xl cursor-pointer">&times </span>
                <p class=" text-center tracking-wide text-xl pb-4"> {{ $header }}</p>
            </div>
        </div>
        <div class="">
            {{ $body }}
        </div>
        <div class="text-center p-5">
            {{ $footer }}
        </div>
        <!--</div>-->
    </div>
</div>