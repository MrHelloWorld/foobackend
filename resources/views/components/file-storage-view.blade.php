<div>
    <div id="{{$uid}}files">
        <div class="flex p-3 border overflow-auto ">
            @if(count($allFiles) == 0)
            Файлы не добавлены
            @endif
            @foreach ($allFiles as $file)
            <div class="border rounded-md text-blue-800 m-2">
                <div class="float-right px-1 text-sm cursor-pointer"
                     href="#"
                     onclick="deleteFile('{{$uid}}','{{ @csrf_token() }}', '{{ $storageType }}' , '{{ $storageFilesStatus }}', '{{ $file['name'] }}', {{ $id }})"
                     >&#10006</div>
                <div class="p-1 m-1 underline">
                    <a class="cursor-pointer"
                       href="{{ Illuminate\Support\Facades\Storage::url($file['name']) }}"
                       download="{{$file['original_name']}}">
                        {{ $file['original_name'] }}
                    </a>
                </div>

            </div>
            @endforeach
        </div>
        <div id="test"></div>
    </div>
    <div id="{{$uid}}filesStatus" class="text-red-500 text-sm">
    </div>
    @if ($needAddButton)
    <input type="file" name="{{$uid}}addedFile" class="p-3 text-center"/>
    <div class="text-center pt-3">
        <x-button type='button'
                  onclick="addFile('{{$uid}}','{{ @csrf_token() }}', '{{ $storageType }}' , '{{ $storageFilesStatus }}', '{{ $id }}')"
                  >
            {{$addButton}}
        </x-button>
    </div>
    @endif
</div>
