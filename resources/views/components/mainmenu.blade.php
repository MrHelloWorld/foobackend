<div class="md:flex rounded-sm bg-gray-10 my-1 px text-black">
    <x-mainmenu-item href="{{ url('/') }}">&#127968;</x-mainmenu-item>
    <x-jet-dropdown align="left">
        <x-slot name='trigger'>
            @if (request()->routeIs('investors*'))
                <x-mainmenu-item class="bg-gray-700 text-white">Пользователи</x-mainmenu-item>
            @else
                <x-mainmenu-item>Пользователи</x-mainmenu-item>
            @endif
        </x-slot>
        <x-slot name='content'>
            <x-mainmenu-link href="{{ route('users.index') }}">Работники</x-mainmenu-link>
            <x-mainmenu-link href="{{ route('investors.index') }}">Инвесторы</x-mainmenu-link>
            <!--<div class="border-t border-gray-300"></div>-->
            <!--<x-mainmenu-link>Создать</x-mainmenu-link>-->
        </x-slot>
    </x-jet-dropdown>

    <x-jet-dropdown align="left">
        <x-slot name='trigger'>
            <x-mainmenu-item class="{{ Request::is('cars*') ? 'bg-gray-700 text-white' : '' }} ">Автомобили
            </x-mainmenu-item>
        </x-slot>
        <x-slot name='content'>
            <x-mainmenu-link href="{{ route('cars.index') }}">Просмотр</x-mainmenu-link>
            <div class="border-t border-gray-300"></div>
            <x-mainmenu-link href="{{ route('cars.add') }}">Добавить</x-mainmenu-link>
        </x-slot>
    </x-jet-dropdown>

    <x-jet-dropdown align="left">
        <x-slot name='trigger'>
            <x-mainmenu-item class="{{ Request::is('projects*') ? 'bg-gray-700 text-white' : '' }} ">Проекты
            </x-mainmenu-item>
        </x-slot>
        <x-slot name='content'>
            <x-mainmenu-link href="{{ route('projects.index') }}">Просмотр</x-mainmenu-link>
            <div class="border-t border-gray-300"></div>
            <x-mainmenu-link href="{{ route('projects.add') }}">Создать</x-mainmenu-link>
        </x-slot>
    </x-jet-dropdown>
    <div class="flex justify-end w-full">
        <x-mainmenu-item href="/investor/">Для инвесторов</x-mainmenu-item>
</div>
</div>
