<x-app-layout>
    <x-page-header>Пользователи</x-page-header>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('users.form')
        </div>
    </div>
</x-app-layout>
