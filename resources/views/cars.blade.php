<x-app-layout>
    <x-page-header>Автомобили</x-page-header>
    <div class="py-12 px-8">
        <livewire:cars.form />
    </div>
</x-app-layout>
