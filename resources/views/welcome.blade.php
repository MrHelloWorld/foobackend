<x-app-layout>
    <x-page-header>Добро пожаловать, {{ Auth()->user()->name }}</x-page-header>
    <div class="flex">
        <div class="p-4 w-1/2">
            <x-forms.form-frame>
                <x-forms.form-caption mainLabel="Ваша статистика" />
                <div class="p-4">
                    <div class="p-2 text-gray-800">
                        Завершено/Всего проектов:
                        <span class="font-bold">
                            {{ App\Services\StatisticsService::getFinishedResaleProjectsCount(false) }}/{{ App\Services\StatisticsService::getResaleProjectsCount(false) }}
                        </span>
                    </div>
                    <div class="p-2 text-gray-800">
                        Завершено/Всего ордеров работ:
                        <span class="font-bold">
                            {{ App\Services\StatisticsService::getFinishedWorkingOrdersCount(false) }}/{{ App\Services\StatisticsService::getWorkingOrdersCount(false) }}
                        </span>
                    </div>
                </div>
            </x-forms.form-frame>
        </div>
        <div class="p-4 w-1/2">
            <x-forms.form-frame>
                <x-forms.form-caption mainLabel="Общая статистика" />
                <div class="p-4">
                    <div class="p-2 text-gray-800">
                        Завершено/Всего проектов:
                        <span class="font-bold">
                            {{ App\Services\StatisticsService::getFinishedResaleProjectsCount() }}/{{ App\Services\StatisticsService::getResaleProjectsCount() }}
                        </span>
                    </div>
                    <div class="p-2 text-gray-800">
                        Завершено/Всего ордеров работ:
                        <span class="font-bold">
                            {{ App\Services\StatisticsService::getFinishedWorkingOrdersCount() }}/{{ App\Services\StatisticsService::getWorkingOrdersCount() }}
                        </span>
                    </div>
                    <div class="p-2 text-gray-800">
                        Работников:
                        <span class="font-bold"> {{ App\Services\StatisticsService::getUsersCount() }}</span>
                    </div>
                    <div class="p-2 text-gray-800">
                        Инвесторов:
                        <span class="font-bold"> {{ App\Services\StatisticsService::getInvestorsCount() }}</span>
                    </div>
                </div>
            </x-forms.form-frame>
        </div>
    </div>
    @php
    //echo phpinfo();
    //dd(request());
    @endphp
</x-app-layout>
