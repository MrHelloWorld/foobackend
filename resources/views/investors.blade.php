<x-app-layout>
    <x-page-header>Инвесторы</x-page-header>
    <div class="py-12 px-8">
        @livewire('investors.form')
    </div>
</x-app-layout>
