<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Foo</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body class="antialiased">
    <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
        <div class="w-full sm:max-w-md mt-2 p-4 bg-white shadow-md sm:rounded-lg tracking-tight relative">
            <div class="font-bold text-lg text-center mb-4">
                Вход
                <a class="absolute cursor-pointer float-right my-auto p-2 right-0 text-blue-700 text-sm top-0 underline" href="/investor/">
                    Для инвеcторов
                </a>
            </div>
            @if (Session::has('status'))
                <div class="text-center text-sm text-red-600">
                    {{ Session::get('status') }}
                </div>

            @else

            @endif
            <form action="login" method="POST">
                @csrf
                <div class="text-gray-700">
                    <div class="flex m-3">
                        <label class="flex-grow m-auto w-2/6"> Почта </label>
                        <input class="w-full form-input" name="email" value="1@2.3" />
                    </div>
                    <div class="flex m-3">
                        <label class="flex-grow m-auto w-2/6"> Пароль </label>
                        <input class="w-full form-input" name="password" type="password" value="1" />
                    </div>
                </div>
                <div class="block m-4 text-right">
                    <label for="remember_me"">
                            <input checked=" true" id="remember" type="checkbox" class="form-checkbox"
                        name="remember" />
                    <span class="ml-2 text-gray-600 ">{{ 'Запомнить' }} </span>
                    </label>
                </div>
                <x-button class='block m-auto'>
                    Войти
                </x-button>
            </form>

        </div>
    </div>

</body>

</html>
