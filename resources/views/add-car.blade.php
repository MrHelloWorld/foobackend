<x-app-layout>
    <x-page-header>Добавление автомобиля</x-page-header>
    {{-- <div class="max-w-full mx-auto sm:px-6 lg:px-8">
        <div class="tracking-tight bg-white shadow-xl rounded-xl border"> --}}
    <x-forms.form-frame>
        <x-forms.form-caption mainLabel="Введите данные" secondLabel="для добавления автомобиля" />
        <form action="/cars/add" method="POST">
            @csrf
            <div class="flex">
                <x-forms.input-error width='w-1/2' labelText="Марка" name="model" />
                <x-forms.input-error width='w-1/2' labelText="Год выпуска" name="year" />
            </div>
            <div class="flex">
                <x-forms.input-error width='w-1/2' labelText="Тип/Категория" name="type" />
                <x-forms.input-error width='w-1/2' labelText="Цвет" name="color" />
            </div>
            <div class="flex">
                <x-forms.input-error width='w-1/2' labelText="Регистрационный знак" name="reg_number" />
                <x-forms.input-error width='w-1/2' labelText="Номер VIN" name="vin" />
            </div>
            <div class="flex">
                <x-forms.input-error width='w-1/2' labelText="Кузов" name="kuzov_number" />
                <x-forms.input-error width='w-1/2' labelText="Шасси" name="rama_number" />
            </div>
            <div class="flex">
                <x-forms.input-error width='w-1/2' labelText="Мощность, лс" name="power_horses" />
                <x-forms.input-error width='w-1/2' labelText="Мощность, квт" name="power_kvt" />
            </div>
            <div class="flex">
                <x-forms.input-error width='w-1/2' labelText="Масса" name="mass" />
                <x-forms.input-error width='w-1/2' labelText="Цена" name="buy_price" />
            </div>

            <div class="text-center p-5">
                <x-button type="submit">Добавить</x-button>
            </div>
        </form>
    </x-form-frame>

    <script>
        Array.prototype.sample = function() {
            return this[Math.floor(Math.random() * this.length)];
        };
        window.onload = function() {
            document.querySelector("input[name='model']").value = ['Жигули', 'Волга', 'Нива'].sample();
            document.querySelector("input[name='year']").value = Math.floor(Math.random() * 200 + 1900);
            document.querySelector("input[name='type']").value = ['Легковая/В', 'Тяжелая/Х', 'Мотоцикл/А'].sample();
            document.querySelector("input[name='color']").value = ['Красный', 'Синий', 'Фиолетовый'].sample();
            document.querySelector("input[name='reg_number']").value = 'ФЫВАПРОЛД'.split('').sample() + ' ' +
                Math.floor(Math.random() * 999) + ' ' +
                'ФЫВАПРОЛД'.split('').sample() +
                'ФЫВАПРОЛД'.split('').sample();
            document.querySelector("input[name='vin']").value = '24323BHJKFL3615KMNBBKSDKLKL';
            document.querySelector("input[name='kuzov_number']").value = 'KJBUT7732487238942387';
            document.querySelector("input[name='rama_number']").value = 'LLJK567324873248972338742';
            document.querySelector("input[name='power_horses']").value = Math.floor(Math.random() * 999);
            document.querySelector("input[name='power_kvt']").value = Math.floor(Math.random() * 1999);
            document.querySelector("input[name='mass']").value = Math.floor(Math.random() * 99) * 500;
            document.querySelector("input[name='buy_price']").value = Math.floor(Math.random() * 99) * 50000 +
                100000;
            document.querySelector("input[name='buy_price']").value = Math.floor(Math.random() * 99) * 50000 +
                100000;
        };

    </script>
    </div>
    </div>
</x-app-layout>
